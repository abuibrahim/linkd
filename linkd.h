/*
 * Copyright (c) 2017 Ruslan Babayev <ruslan@babayev.com>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef LINKD_H
#define LINKD_H

/* system */
int	sys_restart(struct confd_user_info *, struct xml_tag *,
    confd_hkeypath_t *, confd_tag_value_t *, int);
int	sys_shutdown(struct confd_user_info *, struct xml_tag *,
    confd_hkeypath_t *, confd_tag_value_t *, int);
int	sys_setdatetime(struct confd_user_info *, struct xml_tag *,
    confd_hkeypath_t *, confd_tag_value_t *, int);
int	sys_tz_complete(struct confd_user_info *, int, char *, int,
    confd_hkeypath_t *, char *, char *, struct confd_qname *, char *);
int	sys_tz_validate(struct confd_trans_ctx *, confd_hkeypath_t *,
    confd_value_t *);
int	sys_get_elem(struct confd_trans_ctx *, confd_hkeypath_t *);
enum cdb_iter_ret
	sys_iter(confd_hkeypath_t *, enum cdb_iter_op, confd_value_t *,
	    confd_value_t *, void *);

extern int	ds;
extern int	ps;

/* interface */
int	if_init(void);
int	if_get_elem(struct confd_trans_ctx *, confd_hkeypath_t *);
int	if_get_next(struct confd_trans_ctx *, confd_hkeypath_t *, long);
int	if_name_complete(struct confd_user_info *, int, char *, int,
    confd_hkeypath_t *, char *, char *, struct confd_qname *, char *);
enum cdb_iter_ret
	if_iter(confd_hkeypath_t *, enum cdb_iter_op, confd_value_t *,
	    confd_value_t *, void *);
int	ipv4_get_elem(struct confd_trans_ctx *, confd_hkeypath_t *);
int	ipv4_get_next(struct confd_trans_ctx *, confd_hkeypath_t *, long);
int	ipv4_get_case(struct confd_trans_ctx *, confd_hkeypath_t *,
    confd_value_t *);
int	ipv6_get_elem(struct confd_trans_ctx *, confd_hkeypath_t *);
int	ipv6_get_next(struct confd_trans_ctx *, confd_hkeypath_t *, long);

/* routing */
int	rt_init(void);
int	rt_get_elem(struct confd_trans_ctx *, confd_hkeypath_t *);
int	rt_get_next(struct confd_trans_ctx *, confd_hkeypath_t *, long);
int	rt_get_case(struct confd_trans_ctx *, confd_hkeypath_t *,
    confd_value_t *);
int	rt_exists_optional(struct confd_trans_ctx *, confd_hkeypath_t *);
enum cdb_iter_ret
	rt_iter(confd_hkeypath_t *, enum cdb_iter_op, confd_value_t *,
	    confd_value_t *, void *);

struct nlmsghdr;

int	 handle_newlink(struct nlmsghdr *);
int	 handle_dellink(struct nlmsghdr *);
int	 handle_newaddr(struct nlmsghdr *);
int	 handle_deladdr(struct nlmsghdr *);
int	 handle_newneigh(struct nlmsghdr *);
int	 handle_delneigh(struct nlmsghdr *);
int	 handle_newroute(struct nlmsghdr *);
int	 handle_delroute(struct nlmsghdr *);

#endif	/* LINKD_H */
