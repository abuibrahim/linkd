/*
 * Copyright (c) 2017 Ruslan Babayev <ruslan@babayev.com>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <sys/types.h>
#include <sys/wait.h>

#include <linux/netlink.h>
#include <linux/rtnetlink.h>
#include <net/if.h>

#include <string.h>
#include <signal.h>
#include <syslog.h>
#include <err.h>
#include <event.h>
#include <unistd.h>
#include <stdlib.h>

#include <confd_lib.h>
#include <confd_cdb.h>
#include <confd_dp.h>
#include <confd_maapi.h>

#include "linkd.h"
#include "netlink.h"
#include "log.h"
#include "ietf-system.h"
#include "ietf-interfaces.h"
#include "ietf-ip.h"
#include "ietf-routing.h"

#define ARRAY_SIZE(x)	(sizeof(x) / sizeof((x)[0]))

void	sighandler(int, short, void *);
void	usage(void);
void	linkd_shutdown(void);

int	validation_init(struct confd_trans_ctx *);
int	validation_stop(struct confd_trans_ctx *);
int	data_init(struct confd_trans_ctx *);
int	data_finish(struct confd_trans_ctx *);
int	action_init(struct confd_user_info *);
int	sub_diff_iterate(int);
void	sub_handler(int, short, void *);
void	ctl_handler(int, short, void *);
void	wrk_handler(int, short, void *);
void	nl_handler(int, short, void *);
void	wait_phase1(int, short, void *);
void	trigger_all_subscriptions(void);

struct confd_daemon_ctx	*dx;

struct confd_trans_cbs tcbs = {
	.init	= data_init,
	.finish	= data_finish,
};

struct confd_data_cbs dcbs[] = {
	{
		.callpoint	= sys__callpointid_sys_state,
		.get_elem	= sys_get_elem,
	}, {
		.callpoint	= if__callpointid_if_state,
		.get_elem	= if_get_elem,
		.get_next	= if_get_next,
	}, {
		.callpoint	= ip__callpointid_ipv4_state,
		.get_elem	= ipv4_get_elem,
		.get_next	= ipv4_get_next,
		.get_case	= ipv4_get_case,
		.exists_optional= ipv4_get_elem,
	}, {
		.callpoint	= ip__callpointid_ipv6_state,
		.get_elem	= ipv6_get_elem,
		.get_next	= ipv6_get_next,
		.exists_optional= ipv6_get_elem,
	}, {
		.callpoint	= rt__callpointid_routing_state,
		.get_elem	= rt_get_elem,
		.get_next	= rt_get_next,
		.get_case	= rt_get_case,
		.exists_optional= rt_exists_optional,
	}
};

struct confd_action_cbs acbs[] = {
	{
		.actionpoint	= sys__actionpointid_restart,
		.init		= action_init,
		.action		= sys_restart,
	}, {
		.actionpoint	= sys__actionpointid_shutdown,
		.init		= action_init,
		.action		= sys_shutdown,
	}, {
		.actionpoint	= sys__actionpointid_setdatetime,
		.init		= action_init,
		.action		= sys_setdatetime,
	}, {
		.actionpoint	= sys__actionpointid_tz,
		.init		= action_init,
		.completion	= sys_tz_complete,
	}, {
		.actionpoint	= if__actionpointid_if_name,
		.init		= action_init,
		.completion	= if_name_complete,
	}
};

struct confd_trans_validate_cbs vcbs = {
	.init	= validation_init,
	.stop	= validation_stop,
};

struct confd_valpoint_cb valp[] = {
	{
		.valpoint	= sys__validateid_tz,
		.validate	= sys_tz_validate,
	}
};

struct event		 ev_sigint, ev_sigterm, ev_sigchld;
struct event		 ev_sub, ev_ctl, ev_wrk, ev_wait_phase1, ev_nl;
struct sockaddr_in	 confd_addr;
struct sockaddr		*srv = (struct sockaddr *)&confd_addr;
int			 srvsz = sizeof(struct sockaddr_in);
struct timeval		 onesec = { 1, 0 };

typedef enum cdb_iter_ret
	cdb_iter_func(confd_hkeypath_t *, enum cdb_iter_op, confd_value_t *,
	    confd_value_t *, void *);

struct {
	cdb_iter_func	*iter;
	char		*path;
	int		 spoint;
	int		 prio;
} sub[] = {
	{
		.path	= "/system",
		.iter	= sys_iter,
		.prio	= 10,
	}, {
		.path	= "/interfaces",
		.iter	= if_iter,
		.prio	= 20,
	}, {
		.path	= "/routing",
		.iter	= rt_iter,
		.prio	= 30,
	}
};

int	ss;	/* subscription socket */
int	ds;	/* data socket to CDB_RUNNING */
int	ps;	/* data socket to CDB_PRE_COMMIT_RUNNING */
int	cs;	/* control socket */
int	ws;	/* worker socket */
int	ms;	/* maapi socket */
int	ns;	/* netlink socket */

/* ARGSUSED */
void
sighandler(int sig, short event, void *arg)
{
	int wstatus;
	pid_t pid;

	switch (sig) {
	case SIGCHLD:
		pid = wait(&wstatus);
		if (pid < 0)
			fatal("waitpid");
		break;
	case SIGTERM:
	case SIGINT:
		linkd_shutdown();
		break;
	default:
		fatalx("unexpected signal");
		/* NOTREACHED */
	}
}

void
usage(void)
{
	extern char	*__progname;

	fprintf(stderr, "usage: %s [-dv]", __progname);
	exit(1);
}

int
main(int argc, char *argv[])
{
	int	 ch;
	size_t	 i;
	int	 debug = 0;
	int	 verbose = 0;
	char	*path;

	log_init(1, LOG_DAEMON);	/* log to stderr until daemonized */
	log_setverbose(1);

	while ((ch = getopt(argc, argv, "dv")) != -1) {
		switch (ch) {
		case 'd':
			debug = 1;
			break;
		case 'v':
			verbose = 1;
			break;
		default:
			usage();
			/* NOTREACHED */
		}
	}

	argc -= optind;
	argv += optind;
	if (argc > 0)
		usage();

	/* check for root privileges */
	if (geteuid())
		errx(1, "need root privileges");

	log_init(debug, LOG_DAEMON);
	log_setverbose(verbose);

	if (!debug && daemon(1, 0) == -1)
		err(1, "daemon");

	event_init();

	/* setup signal handler */
	signal_set(&ev_sigint, SIGINT, sighandler, NULL);
	signal_set(&ev_sigterm, SIGTERM, sighandler, NULL);
	signal_set(&ev_sigchld, SIGCHLD, sighandler, NULL);
	signal_add(&ev_sigint, NULL);
	signal_add(&ev_sigterm, NULL);
	signal_add(&ev_sigchld, NULL);
	signal(SIGPIPE, SIG_IGN);

	confd_init("linkd", stderr, verbose ? CONFD_TRACE : CONFD_DEBUG);

	confd_addr.sin_family = AF_INET;
	confd_addr.sin_addr.s_addr = htonl(INADDR_LOOPBACK);
	confd_addr.sin_port = htons(CONFD_PORT);

	dx = confd_init_daemon("linkd");
	if (!dx)
		fatal("confd_init_daemon");

	if ((cs = socket(PF_INET, SOCK_STREAM, 0)) < 0)
		fatal("socket");

	if (confd_connect(dx, cs, CONTROL_SOCKET, srv, srvsz) < 0)
		fatal("confd_connect");

	if ((ws = socket(PF_INET, SOCK_STREAM, 0)) < 0)
		fatal("socket");

	if (confd_connect(dx, ws, WORKER_SOCKET, srv, srvsz) < 0)
		fatal("confd_connect");

	confd_register_trans_validate_cb(dx, &vcbs);
	for (i = 0; i < ARRAY_SIZE(valp); i++) {
		if (confd_register_valpoint_cb(dx, &valp[i]) < 0)
			fatal("confd_register_valpoint_cb");
	}

	confd_register_trans_cb(dx, &tcbs);
	for (i = 0; i < ARRAY_SIZE(dcbs); i++) {
		if (confd_register_data_cb(dx, &dcbs[i]) < 0)
			fatal("confd_register_data_cb");
	}

	for (i = 0; i < ARRAY_SIZE(acbs); i++) {
		if (confd_register_action_cbs(dx, &acbs[i]) < 0)
			fatal("confd_register_action_cbs");
	}

	if (confd_register_done(dx) < 0)
		fatal("confd_register_done");

	if ((ms = socket(PF_INET, SOCK_STREAM, 0)) < 0)
		fatal("socket");

	if (maapi_connect(ms, srv, srvsz) < 0)
		fatal("maapi_connect");

	if (maapi_wait_start(ms, 0) < 0)
		fatal("maapi_wait_start");

	log_debug("confd is in phase 0");

	if (maapi_get_schema_file_path(ms, &path) < 0)
		fatal("maapi_get_schema_file_path");

	if (confd_mmap_schemas(path) < 0)
		fatal("confd_mmap_schemas");

	log_debug("memory mapped schemas from %s", path);
	free(path);

	if ((ss = socket(PF_INET, SOCK_STREAM, 0)) < 0)
		fatal("socket");

	if (cdb_connect(ss, CDB_SUBSCRIPTION_SOCKET, srv, srvsz) < 0)
		fatal("cdb_connect");

	if ((ds = socket(PF_INET, SOCK_STREAM, 0)) < 0)
		fatal("socket");

	if (cdb_connect(ds, CDB_DATA_SOCKET, srv, srvsz) < 0)
		fatal("cdb_connect");

	if ((ps = socket(PF_INET, SOCK_STREAM, 0)) < 0)
		fatal("socket");

	if (cdb_connect(ps, CDB_DATA_SOCKET, srv, srvsz) < 0)
		fatal("cdb_connect");

	if ((ns = nl_socket()) < 0)
		fatal("socket");

	if (nl_bind(ns) < 0)
		fatal("nl_bind");

	if (if_init() < 0)
		fatal("if_init");

	if (rt_init() < 0)
		fatal("rt_init");

	event_set(&ev_ctl, cs, EV_READ|EV_PERSIST, ctl_handler, NULL);
	event_add(&ev_ctl, NULL);
	event_set(&ev_wrk, ws, EV_READ|EV_PERSIST, wrk_handler, NULL);
	event_add(&ev_wrk, NULL);
	event_set(&ev_sub, ss, EV_READ|EV_PERSIST, sub_handler, NULL);
	event_add(&ev_sub, NULL);
	event_set(&ev_nl, ns, EV_READ|EV_PERSIST, nl_handler, NULL);
	event_add(&ev_nl, NULL);
	evtimer_set(&ev_wait_phase1, wait_phase1, NULL);
	evtimer_add(&ev_wait_phase1, &onesec);
	event_dispatch();

	linkd_shutdown();

	/* NOTREACHED */
	return (0);
}

void
linkd_shutdown(void)
{
	log_info("terminating");
	cdb_close(ds);
	cdb_close(ps);
	cdb_close(ss);
	cdb_close(ms);
	cdb_close(ws);
	cdb_close(cs);
	confd_release_daemon(dx);
	exit(0);
}

int
validation_init(struct confd_trans_ctx *tctx)
{
	if (maapi_attach(ms, 0, tctx) < 0)
		fatal("maapi_attach");

	confd_trans_set_fd(tctx, ws);
	return CONFD_OK;
}

int
validation_stop(struct confd_trans_ctx *tctx)
{
	if (maapi_detach(ms, tctx) < 0)
		fatal("maapi_detach");

	return CONFD_OK;
}

int
data_init(struct confd_trans_ctx *tctx)
{
	if (maapi_attach(ms, 0, tctx) < 0)
		fatal("maapi_attach");

	confd_trans_set_fd(tctx, ws);
	return CONFD_OK;
}

int
data_finish(struct confd_trans_ctx *tctx)
{
	if (maapi_detach(ms, tctx) < 0)
		fatal("maapi_detach");

	return CONFD_OK;
}

int
action_init(struct confd_user_info *ui)
{
	confd_action_set_fd(ui, ws);
	return CONFD_OK;
}

/* ARGSUSED */
void
ctl_handler(int sock, short event, void *opaque)
{
	int	rv;

	rv = confd_fd_ready(dx, sock);
	if (rv == CONFD_EOF)
		confd_fatal("control socket closed");
	else if (rv == CONFD_ERR && confd_errno != CONFD_ERR_EXTERNAL)
		confd_fatal("error on control socket request");
}

/* ARGSUSED */
void
wrk_handler(int sock, short event, void *opaque)
{
	int	rv;

	rv = confd_fd_ready(dx, sock);
	if (rv == CONFD_EOF)
		confd_fatal("worker socket closed");
	else if (rv == CONFD_ERR && confd_errno != CONFD_ERR_EXTERNAL)
		confd_fatal("error on worker socket request");
}

/* ARGSUSED */
void
sub_handler(int sock, short event, void *opaque)
{
	enum	 cdb_sub_notification type;
	int	 i, n, flags;
	int	*sp;

	if (cdb_read_subscription_socket2(sock, &type, &flags, &sp, &n) < 0)
		fatal("cdb_read_subsription_socket2");

	if (flags & CDB_SUB_FLAG_TRIGGER)
		log_debug("triggered subsription");

	if (n <= 0)
		return;

	if (cdb_start_session(ds, CDB_RUNNING) < 0)
		fatal("cdb_start_session");

	for (i = 0; i < n; i++)
		if (sub_diff_iterate(sp[i]) < 0)
			fatal("sub_diff_iterate");

	if (cdb_end_session(ds) < 0)
		fatal("cdb_end_session");

	if (cdb_sync_subscription_socket(sock, CDB_DONE_PRIORITY) < 0)
		fatal("cdb_sync_subscription_socket");

	free(sp);
}

int
sub_diff_iterate(int spoint)
{
	size_t	i;

	for (i = 0; i < ARRAY_SIZE(sub); i++) {
		if (sub[i].spoint != spoint)
			continue;
		return cdb_diff_iterate(ss, sub[i].spoint, sub[i].iter,
		    0, NULL);
	}

	log_warn("subsription point %d not found", spoint);
	return (0);
}

/* ARGSUSED */
void
nl_handler(int sock, short event, void *opaque)
{
	if (recv_nlmsg(sock) < 0)
		log_warn("nl_handler");
}

/* ARGSUSED */
void
wait_phase1(int sock, short event, void *opaque)
{
	struct cdb_phase	cdb;
	size_t			i;
	int			rv;
	pid_t			pid;

	if (cdb_get_phase(ss, &cdb) < 0)
		fatal("cdb_get_phase");

	if (cdb.phase < 1) {
		log_debug("starting confd phase 1");
		if (maapi_start_phase(ms, 1, 0) < 0)
			fatal("maapi_start_phase");
		evtimer_add(&ev_wait_phase1, &onesec);
		return;
	}

	for (i = 0; i < ARRAY_SIZE(sub); i++) {
		rv = cdb_subscribe(ss, sub[i].prio, 0, &sub[i].spoint,
		    sub[i].path);
		if (rv < 0)
			fatalx("cdb_subscribe");
		log_debug("subscribed to %s at point %d",
		    sub[i].path, sub[i].spoint);
	}

	if (cdb_subscribe_done(ss) < 0)
		fatal("cdb_subscribe_done");

	if (cdb.phase < 2) {
		log_debug("starting confd phase 2");
		if (maapi_start_phase(ms, 2, 1) < 0)
			fatal("maapi_start_phase");
	}

	/*
	 * Trigger subscriptions from the child process since the call
	 * is blocking until all subsribers have acknowledged the
	 * notification.
	 */
	pid = fork();
	if (pid < 0)
		fatal("fork");

	if (pid == 0)	/* child */
		trigger_all_subscriptions();

	log_info("started");
}

void
trigger_all_subscriptions(void)
{
	int sock, err;

	sock = socket(PF_INET, SOCK_STREAM, 0);
	if (sock < 0)
		exit(sock);

	err = cdb_connect(sock, CDB_DATA_SOCKET, srv, srvsz);
	if (err)
		exit(err);

	err = cdb_trigger_subscriptions(sock, NULL, 0);
	if (err)
		exit(err);

	log_info("all subcriptions triggered");
	exit(0);
}
