/*
 * Copyright (c) 2017 Ruslan Babayev <ruslan@babayev.com>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef NETLINK_H
#define NETLINK_H

int	nl_socket(void);
int	nl_bind(int);
int	recv_nlmsg(int);
int	getlink(int);
int	getaddr(int, int);
int	newaddr(int, int, void *, uint8_t);
int	deladdr(int, int, void *, uint8_t);
int	getneigh(int, int);
int	newneigh(int, int, void *, void *);
int	delneigh(int, int, void *, void *);
int	getroute(int, int);
int	newroute(int, int, int, void *, uint8_t, void *, int);
int	delroute(int, int, void *, uint8_t);
int	link_set_flags(int, int);
int	link_set_mtu(int, int);
int	link_set_master(int, int);
int	link_add_vrf(char *, int);
void	rtattr_parse(struct rtattr *, int, struct rtattr **, int);
uint32_t
	rtattr_get_u32(struct rtattr *);
uint8_t	rtattr_get_u8(struct rtattr *);

#endif	/* NETLINK_H */
