/*
 * Copyright (c) 2017 Ruslan Babayev <ruslan@babayev.com>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/queue.h>
#include <sys/tree.h>
#include <sys/socket.h>
#include <sys/sysinfo.h>
#include <net/if_arp.h>
#include <net/if.h>
#include <linux/if.h>
#include <linux/netlink.h>
#include <linux/rtnetlink.h>
#include <linux/ethtool.h>
#include <linux/sockios.h>
#include <net/if.h>

#include <stdarg.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>

#include <confd_lib.h>
#include <confd_cdb.h>
#include <confd_dp.h>

#include "linkd.h"
#include "log.h"
#include "netlink.h"
#include "ietf-interfaces.h"
#include "iana-if-type.h"
#include "ietf-ip.h"
#include "ietf-ipv6-unicast-routing.h"

/* forward declarations */
struct addr;

int	 if_init(void);
int	 if_fetch(int);
int	 if_cmp(struct iface *, struct iface *);
struct iface
	*if_find(int);
struct iface
	*if_insert(int);
int	 if_remove(struct iface *);
struct addr *
	 if_find_ipv4_addr(struct iface *, struct in_addr *);
struct addr *
	 if_find_ipv6_addr(struct iface *, struct in6_addr *);
int	 if_del_ipv4_addr(struct iface *, struct in_addr *);
int	 if_del_ipv6_addr(struct iface *, struct in6_addr *);
struct neighbor *
	 if_find_ipv4_neigh(struct iface *, struct in_addr *);
struct neighbor *
	 if_find_ipv6_neigh(struct iface *, struct in6_addr *);
int	 if_del_ipv4_neigh(struct iface *, struct in_addr *);
int	 if_del_ipv6_neigh(struct iface *, struct in6_addr *);
int	 if_stats(struct iface *, int, confd_value_t *);
int	 if_get_speed(char *, uint64_t *);
int	 if_get_next_if(struct confd_trans_ctx *, long);
int	 if_get_next_higher_layer_if(struct confd_trans_ctx *, long,
    struct iface *);
int	 if_get_next_lower_layer_if(struct confd_trans_ctx *, long,
    struct iface *);

int	 ipv4_get_next_address(struct confd_trans_ctx *, long,
    struct iface *);
int	 ipv4_get_next_neighbor(struct confd_trans_ctx *, long,
    struct iface *);
int	 ipv4_get_elem_address(struct confd_trans_ctx *, confd_hkeypath_t *,
    struct iface *);
int	 ipv4_get_elem_neighbor(struct confd_trans_ctx *, confd_hkeypath_t *,
    struct iface *);

int	 ipv6_get_next_address(struct confd_trans_ctx *, long,
    struct iface *);
int	 ipv6_get_next_neighbor(struct confd_trans_ctx *, long,
    struct iface *);
int	 ipv6_get_elem_address(struct confd_trans_ctx *, confd_hkeypath_t *,
    struct iface *);
int	 ipv6_get_elem_neighbor(struct confd_trans_ctx *, confd_hkeypath_t *,
    struct iface *);
int	 ipv6_get_elem_radv(struct confd_trans_ctx *, confd_hkeypath_t *,
    struct iface *);

int	 ianaift(int);
int	 addr_status(struct addr *);
int	 addr_origin(struct addr *);
int	 neighbor_state(int);
int	 oper_state(int);
int	 proc_get(const char *, ...);
void	 proc_set(int, const char *, ...);

enum cdb_iter_ret
	if_iter_ipv4(confd_hkeypath_t *, enum cdb_iter_op, confd_value_t *,
	    struct iface *);
enum cdb_iter_ret
	if_iter_ipv6(confd_hkeypath_t *, enum cdb_iter_op, confd_value_t *,
	    struct iface *);
void	if_iter_ip_address(confd_hkeypath_t *, enum cdb_iter_op,
    confd_value_t *, int, int, void *);
void	if_iter_ip_neighbor(confd_hkeypath_t *, enum cdb_iter_op,
    confd_value_t *, int, int, void *);
void	if_iter_ipv6_autoconf(int, char *, confd_value_t *);
void	if_iter_ipv6_radv(int, char *, confd_value_t *);

union ip {
	struct in_addr		 v4;
	struct in6_addr		 v6;
};

struct addr {
	TAILQ_ENTRY(addr)	 entry;
	uint8_t			 family;
	uint8_t			 prefixlen;
	uint8_t			 flags;
	uint8_t			 scope;
	union ip		 ip;
};

struct neighbor {
	TAILQ_ENTRY(neighbor)	 entry;
	uint8_t			 family;
	union ip		 ip;
	uint8_t			 lladdr[IFHWADDRLEN];
	int			 state;
	int			 flags;
};

struct iface {
	RB_ENTRY(iface)		 entry;
	TAILQ_HEAD(, addr)	 ipv4_addrs;
	TAILQ_HEAD(, addr)	 ipv6_addrs;
	TAILQ_HEAD(, neighbor)	 ipv4_neighbors;
	TAILQ_HEAD(, neighbor)	 ipv6_neighbors;
	struct iface		*master;
	struct iface		*link;
	struct rtnl_link_stats	 stats;
	time_t			 last_stats;
	char			 name[IFNAMSIZ];
	uint8_t			 hwaddr[IFHWADDRLEN];
	uint8_t			 hwbrd[IFHWADDRLEN];
	int			 index;
	time_t			 last_change;
	uint32_t		 flags;
	uint32_t		 mtu;
	uint32_t		 txqlen;
	uint32_t		 weight;
	uint32_t		 promisc;
	uint16_t		 type;
	uint8_t			 operstate;
};

RB_HEAD(ifaces, iface);
RB_PROTOTYPE(ifaces, iface, entry, if_cmp)
RB_GENERATE(ifaces, iface, entry, if_cmp)

struct {
	struct ifaces		 head;
	int			 nelem;
} iftree;

int
if_cmp(struct iface *a, struct iface *b)
{
	return (a->index - b->index);
}

struct iface *
if_find(int index)
{
	struct iface	s;

	bzero(&s, sizeof(s));
	s.index = index;

	return (RB_FIND(ifaces, &iftree.head, &s));
}

struct iface *
if_insert(int index)
{
	struct iface	*iface;
	struct sysinfo	 info;

	if ((iface = calloc(1, sizeof(struct iface))) == NULL)
		return (NULL);

	if (sysinfo(&info) < 0)
		log_warn("if_insert: sysinfo");

	TAILQ_INIT(&iface->ipv4_addrs);
	TAILQ_INIT(&iface->ipv6_addrs);
	TAILQ_INIT(&iface->ipv4_neighbors);
	TAILQ_INIT(&iface->ipv6_neighbors);
	iface->index = index;
	iface->last_change = time(NULL) - info.uptime;

	if (RB_INSERT(ifaces, &iftree.head, iface) != NULL)
		fatalx("if_insert: RB_INSERT");

	iftree.nelem++;
	return (iface);
}

int
if_remove(struct iface *iface)
{
	struct addr	*addr, *taddr;
	struct neighbor	*neigh, *tneigh;

	if (RB_REMOVE(ifaces, &iftree.head, iface) == NULL) {
		log_warnx("if_remove; interface %d not found",
		    iface->index);
		return (-1);
	}

	TAILQ_FOREACH_SAFE(addr, &iface->ipv4_addrs, entry, taddr) {
		TAILQ_REMOVE(&iface->ipv4_addrs, addr, entry);
		free(addr);
	}

	TAILQ_FOREACH_SAFE(addr, &iface->ipv6_addrs, entry, taddr) {
		TAILQ_REMOVE(&iface->ipv6_addrs, addr, entry);
		free(addr);
	}

	TAILQ_FOREACH_SAFE(neigh, &iface->ipv4_neighbors, entry, tneigh) {
		TAILQ_REMOVE(&iface->ipv4_neighbors, neigh, entry);
		free(neigh);
	}

	TAILQ_FOREACH_SAFE(neigh, &iface->ipv6_neighbors, entry, tneigh) {
		TAILQ_REMOVE(&iface->ipv6_neighbors, neigh, entry);
		free(neigh);
	}

	iftree.nelem--;
	free(iface);
	return (0);
}

int
if_init(void)
{
	RB_INIT(&iftree.head);
	iftree.nelem = 0;
	return if_fetch(0);
}

int
if_fetch(int ifindex)
{
	int	rv;

	rv = getlink(ifindex);
	if (rv < 0)
		return rv;

	rv = getaddr(ifindex, AF_INET);
	if (rv < 0)
		return rv;

	rv = getaddr(ifindex, AF_INET6);
	if (rv < 0)
		return rv;

	rv = getneigh(ifindex, AF_INET);
	if (rv < 0)
		return rv;

	rv = getneigh(ifindex, AF_INET6);
	if (rv < 0)
		return rv;

	return 0;
}

struct addr *
if_find_ipv4_addr(struct iface *iface, struct in_addr *ip)
{
	struct addr	*addr;

	TAILQ_FOREACH(addr, &iface->ipv4_addrs, entry) {
		if (addr->ip.v4.s_addr == ip->s_addr)
			break;
	}

	return addr;
}

struct addr *
if_find_ipv6_addr(struct iface *iface, struct in6_addr *ip)
{
	struct addr	*addr;

	TAILQ_FOREACH(addr, &iface->ipv4_addrs, entry) {
		if (!memcmp(&addr->ip, ip, sizeof(*ip)))
			break;
	}

	return addr;
}

int
if_del_ipv4_addr(struct iface *iface, struct in_addr *ip)
{
	struct addr	*addr, *taddr;
	char		 str[INET_ADDRSTRLEN];

	TAILQ_FOREACH_SAFE(addr, &iface->ipv4_addrs, entry, taddr) {
		if (addr->ip.v4.s_addr == ip->s_addr) {
			TAILQ_REMOVE(&iface->ipv4_addrs, addr, entry);
			free(addr);
			return (0);
		}
	}

	log_warnx("if_del_ipv4_addr: iface:%s addr:%s not found",
	    iface->name, inet_ntop(AF_INET, &ip, str, sizeof str));
	return (0);
}

int
if_del_ipv6_addr(struct iface *iface, struct in6_addr *ip)
{
	struct addr	*addr, *taddr;
	char		 str[INET6_ADDRSTRLEN];

	TAILQ_FOREACH_SAFE(addr, &iface->ipv6_addrs, entry, taddr) {
		if (!memcmp(&addr->ip, ip, sizeof(*ip))) {
			TAILQ_REMOVE(&iface->ipv6_addrs, addr, entry);
			free(addr);
			return (0);
		}
	}

	log_warnx("if_del_ipv6_addr: iface:%s addr:%s not found",
	    iface->name, inet_ntop(AF_INET, &ip, str, sizeof str));
	return (0);
}

struct neighbor *
if_find_ipv4_neigh(struct iface *iface, struct in_addr *ip)
{
	struct neighbor	*neigh;

	TAILQ_FOREACH(neigh, &iface->ipv4_neighbors, entry) {
		if (neigh->ip.v4.s_addr == ip->s_addr)
			break;
	}

	return neigh;
}

struct neighbor *
if_find_ipv6_neigh(struct iface *iface, struct in6_addr *ip)
{
	struct neighbor	*neigh;

	TAILQ_FOREACH(neigh, &iface->ipv6_neighbors, entry) {
		if (!memcmp(&neigh->ip, ip, sizeof(*ip)))
			break;
	}

	return neigh;
}

int
if_del_ipv4_neigh(struct iface *iface, struct in_addr *ip)
{
	struct neighbor	*neigh, *tneigh;

	TAILQ_FOREACH_SAFE(neigh, &iface->ipv4_neighbors, entry, tneigh) {
		if (ip->s_addr == neigh->ip.v4.s_addr) {
			TAILQ_REMOVE(&iface->ipv4_neighbors, neigh, entry);
			free(neigh);
			return (0);
		}
	}

	return (0);
}

int
if_del_ipv6_neigh(struct iface *iface, struct in6_addr *ip)
{
	struct neighbor	*neigh, *tneigh;

	TAILQ_FOREACH_SAFE(neigh, &iface->ipv6_neighbors, entry, tneigh) {
		if (!memcmp(&neigh->ip, ip, sizeof(*ip))) {
			TAILQ_REMOVE(&iface->ipv6_neighbors, neigh, entry);
			free(neigh);
			return (0);
		}
	}

	return (0);
}

int
handle_newlink(struct nlmsghdr *nlmsg)
{
	struct rtattr		*tb[IFLA_MAX + 1];
	struct rtattr		*rta;
	struct ifinfomsg	*ifi;
	struct iface		*iface;
	int			 len;

	len = nlmsg->nlmsg_len - NLMSG_LENGTH(sizeof(*ifi));
	if (len < 0)
		return (-1);

	ifi = NLMSG_DATA(nlmsg);
	rtattr_parse(IFLA_RTA(ifi), len, tb, IFLA_MAX);

	rta = tb[IFLA_IFNAME];
	if (!rta) {
		log_warnx("handle_newlink: missing IFLA_IFNAME attribute");
		return (-1);
	}

	if ((iface = if_find(ifi->ifi_index)) == NULL) {
		if ((iface = if_insert(ifi->ifi_index)) == NULL) {
			log_warnx("handle_newlink: failed inserting "
			    "interface %d", ifi->ifi_index);
			return (-1);
		}
	}

	strlcpy(iface->name, RTA_DATA(rta), sizeof(iface->name));
	iface->type	= ifi->ifi_type;
	iface->mtu	= rtattr_get_u32(tb[IFLA_MTU]);
	iface->txqlen	= rtattr_get_u32(tb[IFLA_TXQLEN]);
	iface->promisc	= rtattr_get_u32(tb[IFLA_PROMISCUITY]);
	iface->weight	= rtattr_get_u32(tb[IFLA_WEIGHT]);
	iface->operstate = rtattr_get_u8(tb[IFLA_OPERSTATE]);
	iface->last_stats = time(NULL);

	if ((iface->flags ^ ifi->ifi_flags) & (IFF_UP | IFF_RUNNING))
		iface->last_change = iface->last_stats;

	iface->flags = ifi->ifi_flags;

	iface->master = if_find(rtattr_get_u32(tb[IFLA_MASTER]));
	iface->link = if_find(rtattr_get_u32(tb[IFLA_LINK]));

	rta = tb[IFLA_ADDRESS];
	if (rta) {
		assert(RTA_PAYLOAD(rta) <= sizeof(iface->hwaddr));
		memcpy(iface->hwaddr, RTA_DATA(rta), RTA_PAYLOAD(rta));
	}

	rta = tb[IFLA_BROADCAST];
	if (rta) {
		assert(RTA_PAYLOAD(rta) <= sizeof(iface->hwbrd));
		memcpy(iface->hwbrd, RTA_DATA(rta), RTA_PAYLOAD(rta));
	}

	rta = tb[IFLA_STATS];
	if (rta) {
		assert(RTA_PAYLOAD(rta) <= sizeof(iface->stats));
		memcpy(&iface->stats, RTA_DATA(rta), RTA_PAYLOAD(rta));
	}

	return (0);
}

int
handle_dellink(struct nlmsghdr *nlmsg)
{
	struct ifinfomsg	*ifi;
	struct iface		*iface;

	if (nlmsg->nlmsg_len < NLMSG_LENGTH(sizeof(*ifi)))
		return (-1);

	ifi = NLMSG_DATA(nlmsg);
	iface = if_find(ifi->ifi_index);
	if (iface)
		if_remove(iface);

	return (0);
}

int
handle_newaddr(struct nlmsghdr *nlmsg)
{
	struct rtattr		*tb[IFA_MAX + 1];
	struct rtattr		*rta;
	struct ifaddrmsg	*ifa;
	struct iface		*iface;
	struct addr		*addr;
	char			*label;
	int			 len;
	char			 str[256];

	len = nlmsg->nlmsg_len - NLMSG_LENGTH(sizeof(*ifa));
	if (len < 0)
		return (-1);

	ifa = NLMSG_DATA(nlmsg);

	iface = if_find(ifa->ifa_index);
	if (!iface) {
		log_warnx("handle_newaddr: interface %d not found",
		    ifa->ifa_index);
		return (-1);
	}

	rtattr_parse(IFA_RTA(ifa), len, tb, IFA_MAX);

	rta = tb[IFA_LABEL];
	if (rta) {
		label = RTA_DATA(rta);
		if (strcmp(iface->name, label)) {
			log_warnx("handle_newaddr: label mismatch: "
			    "expected \"%s\" got \"%s\"",
			    iface->name, label);
			return (-1);
		}
	}

	rta = tb[IFA_ADDRESS];
	if (!rta) {
		log_warnx("handle_newaddr: interface %d missing IFA_ADDRESS",
		    ifa->ifa_index);
		return (-1);
	}

	log_debug("handle_newaddr: %s/%d",
	    inet_ntop(ifa->ifa_family, RTA_DATA(rta), str, sizeof str),
	    ifa->ifa_prefixlen);

	switch (ifa->ifa_family) {
	case AF_INET:
		addr = if_find_ipv4_addr(iface, RTA_DATA(rta));
		break;
	case AF_INET6:
		addr = if_find_ipv6_addr(iface, RTA_DATA(rta));
		break;
	default:
		log_warnx("handle_newaddr: unknown family %d",
		    ifa->ifa_family);
		return (-1);
	}

	if (!addr) {
		addr = calloc(1, sizeof(struct addr));
		if (!addr)
			fatal("handle_newaddr: calloc");
		if (ifa->ifa_family == AF_INET)
			TAILQ_INSERT_TAIL(&iface->ipv4_addrs, addr, entry);
		else
			TAILQ_INSERT_TAIL(&iface->ipv6_addrs, addr, entry);
	}

	addr->family = ifa->ifa_family;
	addr->prefixlen = ifa->ifa_prefixlen;
	addr->flags = ifa->ifa_flags;
	addr->scope = ifa->ifa_scope;
	memcpy(&addr->ip, RTA_DATA(rta), RTA_PAYLOAD(rta));

	return (0);
}

int
handle_deladdr(struct nlmsghdr *nlmsg)
{
	struct rtattr		*tb[IFA_MAX + 1];
	struct rtattr		*rta;
	struct ifaddrmsg	*ifa;
	struct iface		*iface;
	int			 len;
	char			 str[256];

	len = nlmsg->nlmsg_len - NLMSG_LENGTH(sizeof(*ifa));
	if (len < 0)
		return (-1);

	ifa = NLMSG_DATA(nlmsg);

	iface = if_find(ifa->ifa_index);
	if (!iface) {
		log_warnx("handle_deladdr: interface %d not found",
		    ifa->ifa_index);
		return (-1);
	}

	rtattr_parse(IFA_RTA(ifa), len, tb, IFA_MAX);
	rta = tb[IFA_ADDRESS];
	if (!rta) {
		log_warnx("handle_deladdr: interface %d missing IFA_ADDRESS",
		    ifa->ifa_index);
		return (-1);
	}

	log_debug("handle_deladdr: %s/%d",
	    inet_ntop(ifa->ifa_family, RTA_DATA(rta), str, sizeof str),
	    ifa->ifa_prefixlen);

	switch (ifa->ifa_family) {
	case AF_INET:
		return if_del_ipv4_addr(iface, RTA_DATA(rta));
	case AF_INET6:
		return if_del_ipv6_addr(iface, RTA_DATA(rta));
	default:
		log_warnx("handle_deladdr: unknown family %d",
		    ifa->ifa_family);
		return (-1);
	}
}

int
handle_newneigh(struct nlmsghdr *nlmsg)
{
	struct rtattr		*tb[NDA_MAX + 1];
	struct rtattr		*rta, *dst, *lladdr;
	struct ndmsg		*ndm;
	struct iface		*iface;
	struct neighbor		*neigh;
	int			 len;
	char			 str[256];

	len = nlmsg->nlmsg_len - NLMSG_LENGTH(sizeof *ndm);
	if (len < 0)
		return (-1);

	ndm = NLMSG_DATA(nlmsg);

	iface = if_find(ndm->ndm_ifindex);
	if (!iface) {
		log_warnx("handle_newneigh: interface %d not found",
		    ndm->ndm_ifindex);
		return (-1);
	}

	if (ndm->ndm_state == NUD_FAILED) {
		log_debug("handle_newneigh: interface %d failed neighbor",
		    ndm->ndm_ifindex);
		return (0);
	}

	rta = (struct rtattr *)((char*)ndm + NLMSG_ALIGN(sizeof *ndm));
	rtattr_parse(rta, len, tb, NDA_MAX);

	dst = tb[NDA_DST];
	if (!dst) {
		log_warnx("handle_newneigh: interface %d missing NDA_DST",
		    ndm->ndm_ifindex);
		return (-1);
	}

	log_debug("handle_newneigh: %s state:0x%x flags:0x%x",
	    inet_ntop(ndm->ndm_family, RTA_DATA(dst), str, sizeof str),
	    ndm->ndm_state, ndm->ndm_flags);

	lladdr = tb[NDA_LLADDR];
	if (!lladdr) {
		log_warnx("handle_newneigh: interface %d missing NDA_LLADDR",
		    ndm->ndm_ifindex);
		return (-1);
	}

	switch (ndm->ndm_family) {
	case AF_INET:
		neigh = if_find_ipv4_neigh(iface, RTA_DATA(dst));
		break;
	case AF_INET6:
		neigh = if_find_ipv6_neigh(iface, RTA_DATA(dst));
		break;
	default:
		log_warnx("handle_newneigh: unknown family %d",
		    ndm->ndm_family);
		return (-1);
	}

	if (!neigh) {
		neigh = calloc(1, sizeof *neigh);
		if (!neigh)
			fatal("handle_newneigh: calloc");
		if (ndm->ndm_family == AF_INET)
			TAILQ_INSERT_TAIL(&iface->ipv4_neighbors, neigh,
			    entry);
		else
			TAILQ_INSERT_TAIL(&iface->ipv6_neighbors, neigh,
			    entry);

	}

	neigh->family = ndm->ndm_family;
	neigh->state = ndm->ndm_state;
	neigh->flags = ndm->ndm_flags;
	memcpy(&neigh->ip, RTA_DATA(dst), RTA_PAYLOAD(dst));
	memcpy(&neigh->lladdr, RTA_DATA(lladdr), IFHWADDRLEN);

	return (0);
}

int
handle_delneigh(struct nlmsghdr *nlmsg)
{
	struct rtattr		*tb[NDA_MAX + 1];
	struct rtattr		*rta;
	struct ndmsg		*ndm;
	struct iface		*iface;
	int			 len;
	char			 str[256];

	len = nlmsg->nlmsg_len - NLMSG_LENGTH(sizeof *ndm);
	if (len < 0)
		return (-1);

	ndm = NLMSG_DATA(nlmsg);

	iface = if_find(ndm->ndm_ifindex);
	if (!iface) {
		log_warnx("handle_delneigh: interface %d not found",
		    ndm->ndm_ifindex);
		return (-1);
	}

	rta = (struct rtattr *)((char*)ndm + NLMSG_ALIGN(sizeof *ndm));
	rtattr_parse(rta, len, tb, NDA_MAX);

	rta = tb[NDA_DST];
	if (!rta) {
		log_warnx("handle_delneigh: interface %d missing NDA_DST",
		    ndm->ndm_ifindex);
		return (-1);
	}

	log_debug("handle_delneigh: %s state:0x%x flags:0x%x",
	    inet_ntop(ndm->ndm_family, RTA_DATA(rta), str, sizeof str),
	    ndm->ndm_state, ndm->ndm_flags);

	switch (ndm->ndm_family) {
	case AF_INET:
		return if_del_ipv4_neigh(iface, RTA_DATA(rta));
	case AF_INET6:
		return if_del_ipv6_neigh(iface, RTA_DATA(rta));
	default:
		log_warnx("handle_delneigh: unknown family %d",
		    ndm->ndm_family);
		return (-1);
	}
}

/* ARGSUSED */
enum cdb_iter_ret
if_iter(confd_hkeypath_t *kp, enum cdb_iter_op op, confd_value_t *oldv,
    confd_value_t *newv, void *state)
{
	struct		 confd_identityref type;
	struct iface	*iface;
	int		 tag, i = kp->len;
	int		 enabled;
	unsigned int	 ifindex;
	char		*ifname;

	--i; tag = CONFD_GET_XMLTAG(&kp->v[i][0]);
	if (tag != if_interfaces)
		return ITER_CONTINUE;

	--i; tag = CONFD_GET_XMLTAG(&kp->v[i][0]);
	if (tag != if_interface)
		return ITER_CONTINUE;

	--i; ifname = CONFD_GET_CBUFPTR(&kp->v[i][0]);
	ifindex = if_nametoindex(ifname);
	if (!ifindex)
		return ITER_CONTINUE;

	iface = if_find(ifindex);
	if (!iface)
		return ITER_CONTINUE;

	if (i < 1) {
		if (op == MOP_DELETED) {
			if (link_set_flags(ifindex, 0) < 0) {
				log_warnx("if_iter: link_set_flags");
				return ITER_STOP;
			}
			return ITER_CONTINUE;
		}
		if (cdb_pushd(ds, "%h", kp) < 0) {
			log_warnx("if_iter: cdb_pushd");
			return ITER_STOP;
		}
		if (cdb_get_bool(ds, &enabled, "enabled") < 0) {
			log_warnx("if_iter: cdb_get_bool");
			return ITER_STOP;
		}
		if (cdb_get_identityref(ds, &type, "type") < 0) {
			log_warnx("if_iter: cdb_get_identityref");
			return ITER_STOP;
		}
		if (type.ns != ianaift__ns)
			fatal("if_iter: unknown type.ns %d", type.ns);
		if (link_set_flags(ifindex, enabled ? IFF_UP : 0) < 0)
			fatal("if_iter: link_set_flags");
		cdb_popd(ds);
		return ITER_RECURSE;
	}

	--i; tag = CONFD_GET_XMLTAG(&kp->v[i][0]);
	switch (tag) {
	case ip_ipv4:
		return if_iter_ipv4(kp, op, newv, iface);
	case ip_ipv6:
		return if_iter_ipv6(kp, op, newv, iface);
	}

	return ITER_CONTINUE;
}

enum cdb_iter_ret
if_iter_ipv4(confd_hkeypath_t *kp, enum cdb_iter_op op, confd_value_t *newv,
    struct iface *iface)
{
	struct in_addr	 ip4;
	int		 val, tag, i = kp->len - 4;

	if (i < 1)
		return ITER_RECURSE;

	--i; tag = CONFD_GET_XMLTAG(&kp->v[i][0]);
	switch (tag) {
	case ip_enabled:
		val = CONFD_GET_BOOL(newv);
		if (!val)
			log_warnx("cannot disable IPv4");
		break;
	case ip_forwarding:
		val = CONFD_GET_BOOL(newv);
		proc_set(val, "/proc/sys/net/ipv4/conf/%s/forwarding",
		    iface->name);
		break;
	case ip_mtu:
		val = CONFD_GET_UINT16(newv);
		if (link_set_mtu(iface->index, val) < 0) {
			log_warnx("if_iter: link_set_mtu");
			return ITER_STOP;
		}
		break;
	case ip_address:
		--i; ip4 = CONFD_GET_IPV4(&kp->v[i][0]);
		if_iter_ip_address(kp, op, newv, iface->index, AF_INET, &ip4);
		break;
	case ip_neighbor:
		--i; ip4 = CONFD_GET_IPV4(&kp->v[i][0]);
		if_iter_ip_neighbor(kp, op, newv, iface->index, AF_INET, &ip4);
		break;
	}

	return ITER_CONTINUE;
}

void
if_iter_ip_address(confd_hkeypath_t *kp, enum cdb_iter_op op,
    confd_value_t *newv, int ifindex, int family, void *addr)
{
	uint8_t		 prefixlen;

	if (op == MOP_DELETED) {
		if (cdb_start_session(ps, CDB_PRE_COMMIT_RUNNING) < 0)
			fatal("if_iter_ip_address: cdb_start_session");

		cdb_pushd(ps, "%h", kp);
		if (cdb_get_u_int8(ps, &prefixlen, "prefix-length") < 0)
			fatal("if_iter_ip_address: prefix-length");
		cdb_popd(ps);

		if (cdb_end_session(ps) < 0)
			fatal("if_iter_ip_address: cdb_end_session");

		if (deladdr(ifindex, family, addr, prefixlen) < 0)
			fatal("if_iter_ip_address: deladdr");
		return;
	}

	cdb_pushd(ds, "%h", kp);
	if (cdb_get_u_int8(ds, &prefixlen, "prefix-length") < 0)
		fatal("if_iter_ip_address: prefix-length");
	cdb_popd(ds);

	if (newaddr(ifindex, family, addr, prefixlen) < 0)
		log_warn("if_iter_ip_address: newaddr");
}

void
if_iter_ip_neighbor(confd_hkeypath_t *kp, enum cdb_iter_op op,
    confd_value_t *newv, int ifindex, int family, void *addr)
{
	uint8_t		*lladdr;
	int		 lladdrsz;

	if (op == MOP_DELETED) {
		if (delneigh(ifindex, family, addr, NULL) < 0)
			fatal("if_iter: delneigh");
		return;
	}

	cdb_pushd(ds, "%h", kp);
	if (cdb_get_binary(ds, &lladdr, &lladdrsz, "link-layer-address") < 0)
		fatal("if_iter: cdb_get_binary");
	cdb_popd(ds);
	if (newneigh(ifindex, family, addr, lladdr) < 0)
		log_warn("if_iter: newneigh");
	free(lladdr);
}

enum cdb_iter_ret
if_iter_ipv6(confd_hkeypath_t *kp, enum cdb_iter_op op, confd_value_t *newv,
    struct iface *iface)
{
	struct in6_addr	 ip6;
	int		 val, tag, i = kp->len - 4;

	if (i < 1)
		return ITER_RECURSE;

	--i; tag = CONFD_GET_XMLTAG(&kp->v[i][0]);
	switch (tag) {
	case ip_enabled:
		val = CONFD_GET_BOOL(newv);
		proc_set(!val, "/proc/sys/net/ipv6/conf/%s/disable_ipv6",
		    iface->name);
		break;
	case ip_forwarding:
		val = CONFD_GET_BOOL(newv);
		proc_set(val, "/proc/sys/net/ipv6/conf/%s/forwarding",
		    iface->name);
		break;
	case ip_mtu:
		val = CONFD_GET_UINT32(newv);
		proc_set(val, "/proc/sys/net/ipv6/conf/%s/mtu",
		    iface->name);
		break;
	case ip_dup_addr_detect_transmits:
		val = CONFD_GET_UINT32(newv);
		proc_set(val, "/proc/sys/net/ipv6/conf/%s/dad_transmits",
		    iface->name);
		break;
	case ip_address:
		--i; ip6 = CONFD_GET_IPV6(&kp->v[i][0]);
		if_iter_ip_address(kp, op, newv, iface->index,
		    AF_INET6, &ip6);
		break;
	case ip_neighbor:
		--i; ip6 = CONFD_GET_IPV6(&kp->v[i][0]);
		if_iter_ip_neighbor(kp, op, newv, iface->index,
		    AF_INET6, &ip6);
		break;
	case ip_autoconf:
		--i; tag = CONFD_GET_XMLTAG(&kp->v[i][0]);
		if_iter_ipv6_autoconf(tag, iface->name, newv);
		break;
	case v6ur_ipv6_router_advertisements:
		--i; tag = CONFD_GET_XMLTAG(&kp->v[i][0]);
		if_iter_ipv6_radv(tag, iface->name, newv);
		break;
	}

	return ITER_CONTINUE;
}

void
if_iter_ipv6_autoconf(int tag, char *ifname, confd_value_t *newv)
{
	int	val;

	switch (tag) {
	case ip_create_global_addresses:
		val = CONFD_GET_BOOL(newv);
		proc_set(val, "/proc/sys/net/ipv6/conf/%s/autoconf",
		    ifname);
		break;
	case ip_create_temporary_addresses:
		val = CONFD_GET_BOOL(newv);
		proc_set(val, "/proc/sys/net/ipv6/conf/%s/use_tempaddr",
		    ifname);
		break;
	case ip_temporary_valid_lifetime:
		val = CONFD_GET_UINT32(newv);
		proc_set(val, "/proc/sys/net/ipv6/conf/%s/temp_valid_lft",
		    ifname);
		break;
	case ip_temporary_preferred_lifetime:
		val = CONFD_GET_UINT32(newv);
		proc_set(val, "/proc/sys/net/ipv6/conf/%s/temp_prefered_lft",
		    ifname);
		break;
	}
}

void
if_iter_ipv6_radv(int tag, char *ifname, confd_value_t *newv)
{
	int	val;

	switch (tag) {
	case v6ur_retrans_timer:
		val = CONFD_GET_UINT32(newv);
		proc_set(val, "/proc/sys/net/ipv6/neigh/%s/"
		    "retrans_time_ms", ifname);
		break;
	case v6ur_reachable_time:
		val = CONFD_GET_UINT32(newv);
		proc_set(val, "/proc/sys/net/ipv6/neigh/%s/"
		    "base_reachable_time_ms", ifname);
		break;
	case v6ur_cur_hop_limit:
		val = CONFD_GET_UINT8(newv);
		proc_set(val, "/proc/sys/net/ipv6/conf/%s/"
		    "hop_limit", ifname);
		break;
	case v6ur_link_mtu:
		val = CONFD_GET_UINT32(newv);
		proc_set(val, "/proc/sys/net/ipv6/conf/%s/"
		    "mtu", ifname);
		break;
	}
}

/* ARGSUSED */
int
if_get_next(struct confd_trans_ctx *tctx, confd_hkeypath_t *kp, long next)
{
	struct iface	*iface;
	char		*ifname;
	unsigned int	 ifindex;
	int		 tag, i = kp->len;

	--i; tag = CONFD_GET_XMLTAG(&kp->v[i][0]);
	if (tag != if_interfaces)
		goto not_found;

	--i; tag = CONFD_GET_XMLTAG(&kp->v[i][0]);
	if (tag != if_interface)
		goto not_found;

	if (i < 1)
		return if_get_next_if(tctx, next);

	--i; ifname = CONFD_GET_CBUFPTR(&kp->v[i][0]);
	ifindex = if_nametoindex(ifname);
	if (!ifindex)
		goto not_found;

	iface = if_find(ifindex);
	if (!iface)
		goto not_found;

	--i; tag = CONFD_GET_XMLTAG(&kp->v[i][0]);
	switch (tag) {
	case if_higher_layer_if:
		return if_get_next_higher_layer_if(tctx, next, iface);
	case if_lower_layer_if:
		return if_get_next_lower_layer_if(tctx, next, iface);
	}

not_found:
	confd_data_reply_next_key(tctx, NULL, 0, 0);
	return CONFD_OK;
}

int
if_get_next_if(struct confd_trans_ctx *tctx, long next)
{
	struct iface	*iface;
	confd_value_t	 v;

	if (next == -1)
		iface = RB_MIN(ifaces, &iftree.head);
	else
		iface = (struct iface *)next;

	if (!iface) {
		confd_data_reply_next_key(tctx, NULL, 0, 0);
		return CONFD_OK;
	}

	CONFD_SET_STR(&v, iface->name);
	next = (long)RB_NEXT(ifaces, &iftree.head, iface);
	confd_data_reply_next_key(tctx, &v, 1, next);
	return CONFD_OK;
}

int
if_get_next_higher_layer_if(struct confd_trans_ctx *tctx, long next,
    struct iface *iface)
{
	confd_value_t	 v;

	if (next == -1)
		iface = iface->master;
	else
		iface = (struct iface *)next;

	if (!iface) {
		confd_data_reply_next_key(tctx, NULL, 0, 0);
		return CONFD_OK;
	}

	CONFD_SET_STR(&v, iface->name);
	next = (long)iface->master;
	confd_data_reply_next_key(tctx, &v, 1, next);
	return CONFD_OK;
}

int
if_get_next_lower_layer_if(struct confd_trans_ctx *tctx, long next,
    struct iface *iface)
{
	confd_value_t	 v;

	if (next == -1)
		iface = iface->link;
	else
		iface = (struct iface *)next;

	if (!iface) {
		confd_data_reply_next_key(tctx, NULL, 0, 0);
		return CONFD_OK;
	}

	CONFD_SET_STR(&v, iface->name);
	next = (long)iface->link;
	confd_data_reply_next_key(tctx, &v, 1, next);
	return CONFD_OK;
}

int
if_get_elem(struct confd_trans_ctx *tctx, confd_hkeypath_t *kp)
{
	struct		 confd_identityref idref;
	struct iface	*iface;
	struct		 confd_datetime dt;
	struct tm	*tm;
	confd_value_t	 v;
	int		 tag, i = kp->len;
	char		*ifname;
	unsigned int	 ifindex;
	uint64_t	 speed;

	--i; tag = CONFD_GET_XMLTAG(&kp->v[i][0]);
	if (tag != if_interfaces)
		goto not_found;

	--i; tag = CONFD_GET_XMLTAG(&kp->v[i][0]);
	if (tag != if_interface)
		goto not_found;

	--i; ifname = CONFD_GET_CBUFPTR(&kp->v[i][0]);
	ifindex = if_nametoindex(ifname);
	if (!ifindex)
		goto not_found;

	iface = if_find(ifindex);
	if (!iface)
		goto not_found;

	--i; tag = CONFD_GET_XMLTAG(&kp->v[i][0]);
	switch (tag) {
	case if_name:
		CONFD_SET_STR(&v, ifname);
		break;
	case if_type:
		idref.ns = ianaift__ns;
		idref.id = ianaift(iface->type);
		CONFD_SET_IDENTITYREF(&v, idref);
		break;
	case if_admin_status:
		if (iface->flags & IFF_UP)
			CONFD_SET_ENUM_VALUE(&v, if_up);
		else
			CONFD_SET_ENUM_VALUE(&v, if_down);
		break;
	case if_oper_status:
		CONFD_SET_ENUM_VALUE(&v, oper_state(iface->operstate));
		break;
	case if_last_change:
		if (!iface->last_change)
			goto not_found;

		tm = localtime(&iface->last_change);
		if (!tm)
			fatal("if_get_elem: localtime");

		dt.year		= tm->tm_year + 1900;
		dt.month	= tm->tm_mon + 1;
		dt.day		= tm->tm_mday;
		dt.hour		= tm->tm_hour;
		dt.min		= tm->tm_min;
		dt.sec		= tm->tm_sec;
		dt.micro	= 0;
		dt.timezone	= timezone / 3600;
		dt.timezone_minutes = (timezone % 3600) / 60;
		CONFD_SET_DATETIME(&v, dt);
		break;
	case if_if_index:
		CONFD_SET_INT32(&v, iface->index);
		break;
	case if_phys_address:
		CONFD_SET_BINARY(&v, iface->hwaddr, IFHWADDRLEN);
		break;
	case if_higher_layer_if:
		CONFD_SET_STR(&v, iface->master->name);
		break;
	case if_lower_layer_if:
		CONFD_SET_STR(&v, iface->link->name);
	case if_speed:
		if (if_get_speed(ifname, &speed) < 0)
			goto not_found;
		else
			CONFD_SET_UINT64(&v, speed);
		break;
	case if_statistics:
		if (time(NULL) > iface->last_stats) {
			if (getlink(ifindex) < 0) {
				log_warnx("if_get_elem: getlink");
				goto not_found;
			}
		}
		--i; tag = CONFD_GET_XMLTAG(&kp->v[i][0]);
		if (if_stats(iface, tag, &v) < 0)
			goto not_found;
		break;
	default:
		goto not_found;
	}
	confd_data_reply_value(tctx, &v);
	return CONFD_OK;

not_found:
	confd_data_reply_not_found(tctx);
	return CONFD_OK;
}

int
if_stats(struct iface *iface, int tag, confd_value_t *v)
{

	switch (tag) {
	case if_in_octets:
		CONFD_SET_UINT64(v, iface->stats.rx_bytes);
		break;
	case if_in_unicast_pkts:
		CONFD_SET_UINT64(v, iface->stats.rx_packets);
		break;
	case if_in_multicast_pkts:
		CONFD_SET_UINT64(v, iface->stats.multicast);
		break;
	case if_in_discards:
		CONFD_SET_UINT32(v, iface->stats.rx_dropped);
		break;
	case if_in_errors:
		CONFD_SET_UINT32(v, iface->stats.rx_errors);
		break;
	case if_out_octets:
		CONFD_SET_UINT64(v, iface->stats.tx_bytes);
		break;
	case if_out_unicast_pkts:
		CONFD_SET_UINT64(v, iface->stats.tx_packets);
		break;
	case if_out_discards:
		CONFD_SET_UINT32(v, iface->stats.tx_dropped);
		break;
	case if_out_errors:
		CONFD_SET_UINT32(v, iface->stats.tx_errors);
		break;
	default:
		return CONFD_ERR;
	}

	return CONFD_OK;
}

int
if_get_speed(char *ifname, uint64_t *retval)
{
	struct ifreq		ifr;
	struct ethtool_cmd	ecmd;
	int			sock, err, speed;

	memset(&ifr, 0, sizeof ifr);
	strlcpy(ifr.ifr_name, ifname, sizeof ifr.ifr_name);
	ifr.ifr_data = (void *)&ecmd;
	ecmd.cmd = ETHTOOL_GSET;

	if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
		fatal("iface_speed: socket");

	err = ioctl(sock, SIOCETHTOOL, &ifr);
	if (err)
		goto out;

	speed = ethtool_cmd_speed(&ecmd);
	if (speed == SPEED_UNKNOWN) {
		close(sock);
		return (-1);
	}
	*retval = speed * 1000000;
out:
	close(sock);
	return (err);
}

int
ianaift(int type)
{
	switch (type) {
	case ARPHRD_ETHER:
		return ianaift_ethernetCsmacd;
	case ARPHRD_TUNNEL:
	case ARPHRD_TUNNEL6:
	case ARPHRD_SIT:
		return ianaift_tunnel;
	case ARPHRD_SLIP:
	case ARPHRD_CSLIP:
	case ARPHRD_SLIP6:
	case ARPHRD_CSLIP6:
		return ianaift_slip;
	case ARPHRD_PPP:
		return ianaift_ppp;
	case ARPHRD_LOOPBACK:
		return ianaift_softwareLoopback;
	case ARPHRD_FDDI:
		return ianaift_fddi;
	case ARPHRD_ARCNET:
		return ianaift_arcnet;
	case ARPHRD_LOCALTLK:
		return ianaift_localTalk;
	case ARPHRD_HIPPI:
		return ianaift_hippi;
	case ARPHRD_ATM:
		return ianaift_atm;
	default:
		return ianaift_other;
	}
}

/* ARGSUSED */
int
if_name_complete(struct confd_user_info *uinfo, int cli_style, char *token,
    int completion_char, confd_hkeypath_t *kp, char *cmdpath,
    char *cmdparam_id, struct confd_qname *simple_type, char *extra)
{
	struct	confd_completion_value *cv;
	struct	iface *iface;
	int	i;

	cv = calloc(iftree.nelem, sizeof *cv);
	if (!cv)
		fatal("if_name_complete: calloc");

	i = 0;
	RB_FOREACH(iface, ifaces, &iftree.head) {
		cv[i].type = CONFD_COMPLETION;
		cv[i].value = strdup(iface->name);
		cv[i].extra = NULL;
		i++;
	}

	if (confd_action_reply_completion(uinfo, cv, i) < 0)
		fatal("if_name_complete: confd_action_reply_completion");

	for (--i; i >= 0; i--)
		free(cv[i].value);

	free(cv);
	return CONFD_OK;
}

int
ipv4_get_next_address(struct confd_trans_ctx *tctx, long next,
    struct iface *iface)
{
	struct addr	*addr;
	confd_value_t	 v;

	if (next == -1)
		addr = TAILQ_FIRST(&iface->ipv4_addrs);
	else
		addr = (struct addr *)next;

	if (!addr) {
		confd_data_reply_next_key(tctx, NULL, 0, 0);
	} else {
		CONFD_SET_IPV4(&v, addr->ip.v4);
		next = (long)TAILQ_NEXT(addr, entry);
		confd_data_reply_next_key(tctx, &v, 1, next);
	}

	return CONFD_OK;
}

int
ipv6_get_next_address(struct confd_trans_ctx *tctx, long next,
    struct iface *iface)
{
	struct addr	*addr;
	confd_value_t	 v;

	if (next == -1)
		addr = TAILQ_FIRST(&iface->ipv6_addrs);
	else
		addr = (struct addr *)next;

	if (!addr) {
		confd_data_reply_next_key(tctx, NULL, 0, 0);
	} else {
		CONFD_SET_IPV6(&v, addr->ip.v6);
		next = (long)TAILQ_NEXT(addr, entry);
		confd_data_reply_next_key(tctx, &v, 1, next);
	}

	return CONFD_OK;
}

int
ipv4_get_next_neighbor(struct confd_trans_ctx *tctx, long next,
    struct iface *iface)
{
	struct neighbor	*neigh;
	confd_value_t	 v;

	if (next == -1)
		neigh = TAILQ_FIRST(&iface->ipv4_neighbors);
	else
		neigh = (struct neighbor *)next;

	if (!neigh) {
		confd_data_reply_next_key(tctx, NULL, 0, 0);
	} else {
		CONFD_SET_IPV4(&v, neigh->ip.v4);
		next = (long)TAILQ_NEXT(neigh, entry);
		confd_data_reply_next_key(tctx, &v, 1, next);
	}

	return CONFD_OK;
}

int
ipv6_get_next_neighbor(struct confd_trans_ctx *tctx, long next,
    struct iface *iface)
{
	struct neighbor	*neigh;
	confd_value_t	 v;

	if (next == -1)
		neigh = TAILQ_FIRST(&iface->ipv6_neighbors);
	else
		neigh = (struct neighbor *)next;

	if (!neigh) {
		confd_data_reply_next_key(tctx, NULL, 0, 0);
	} else {
		CONFD_SET_IPV6(&v, neigh->ip.v6);
		next = (long)TAILQ_NEXT(neigh, entry);
		confd_data_reply_next_key(tctx, &v, 1, next);
	}

	return CONFD_OK;
}

int
ipv4_get_next(struct confd_trans_ctx *tctx, confd_hkeypath_t *kp, long next)
{
	struct iface		*iface;
	int			 tag, i = kp->len;
	char			*ifname;
	unsigned int		 ifindex;

	--i; tag = CONFD_GET_XMLTAG(&kp->v[i][0]);
	if (tag != if_interfaces)
		goto not_found;

	--i; tag = CONFD_GET_XMLTAG(&kp->v[i][0]);
	if (tag != if_interface)
		goto not_found;

	--i; ifname = CONFD_GET_CBUFPTR(&kp->v[i][0]);
	ifindex = if_nametoindex(ifname);
	if (!ifindex)
		goto not_found;

	iface = if_find(ifindex);
	if (!iface)
		goto not_found;

	--i; tag = CONFD_GET_XMLTAG(&kp->v[i][0]);
	if (tag != ip_ipv4)
		goto not_found;

	--i; tag = CONFD_GET_XMLTAG(&kp->v[i][0]);
	switch (tag) {
	case ip_address:
		return ipv4_get_next_address(tctx, next, iface);
	case ip_neighbor:
		return ipv4_get_next_neighbor(tctx, next, iface);
	}

not_found:
	confd_data_reply_next_key(tctx, NULL, 0, 0);
	return CONFD_OK;
}

int
ipv4_get_elem(struct confd_trans_ctx *tctx, confd_hkeypath_t *kp)
{
	struct iface		*iface;
	confd_value_t		 v;
	int			 val, tag, i = kp->len;
	char			*ifname;
	unsigned int		 ifindex;

	--i; tag = CONFD_GET_XMLTAG(&kp->v[i][0]);
	if (tag != if_interfaces)
		goto not_found;

	--i; tag = CONFD_GET_XMLTAG(&kp->v[i][0]);
	if (tag != if_interface)
		goto not_found;

	--i; ifname = CONFD_GET_CBUFPTR(&kp->v[i][0]);
	ifindex = if_nametoindex(ifname);
	if (!ifindex)
		goto not_found;

	iface = if_find(ifindex);
	if (!iface)
		goto not_found;

	--i; tag = CONFD_GET_XMLTAG(&kp->v[i][0]);
	if (tag != ip_ipv4)
		goto not_found;

	if (i < 1) {	/* ipv4 presence container */
		if (iface->flags & IFF_UP)
			confd_data_reply_found(tctx);
		else
			confd_data_reply_not_found(tctx);
		return CONFD_OK;
	}

	--i; tag = CONFD_GET_XMLTAG(&kp->v[i][0]);
	switch (tag) {
	case ip_mtu:
		CONFD_SET_UINT16(&v, iface->mtu);
		break;
	case ip_forwarding:
		val = proc_get("/proc/sys/net/ipv4/conf/%s/forwarding",
		    ifname);
		CONFD_SET_BOOL(&v, val);
		break;
	case ip_address:
		return ipv4_get_elem_address(tctx, kp, iface);
	case ip_neighbor:
		return ipv4_get_elem_neighbor(tctx, kp, iface);
	default:
		goto not_found;
	}
	confd_data_reply_value(tctx, &v);
	return CONFD_OK;

not_found:
	confd_data_reply_not_found(tctx);
	return CONFD_OK;
}

int
ipv4_get_elem_address(struct confd_trans_ctx *tctx, confd_hkeypath_t *kp,
    struct iface *iface)
{
	struct addr	*addr;
	struct in_addr	 ip;
	confd_value_t	 v;
	int		 tag, i = kp->len - 5;

	--i; ip = CONFD_GET_IPV4(&kp->v[i][0]);
	TAILQ_FOREACH(addr, &iface->ipv4_addrs, entry) {
		if (addr->ip.v4.s_addr == ip.s_addr)
			break;
	}
	if (!addr)
		goto not_found;

	--i; tag = CONFD_GET_XMLTAG(&kp->v[i][0]);
	switch (tag) {
	case ip_ip:
		CONFD_SET_IPV4(&v, ip);
		break;
	case ip_prefix_length:
		CONFD_SET_UINT8(&v, addr->prefixlen);
		break;
	case ip_origin:
		CONFD_SET_ENUM_VALUE(&v, addr_origin(addr));
		break;
	default:
		goto not_found;
	}
	confd_data_reply_value(tctx, &v);
	return CONFD_OK;

not_found:
	confd_data_reply_not_found(tctx);
	return CONFD_OK;
}

int
ipv4_get_elem_neighbor(struct confd_trans_ctx *tctx, confd_hkeypath_t *kp,
    struct iface *iface)
{
	struct neighbor	*neigh;
	struct in_addr	 ip;
	confd_value_t	 v;
	int		 tag, i = kp->len - 5;

	--i; ip = CONFD_GET_IPV4(&kp->v[i][0]);
	TAILQ_FOREACH(neigh, &iface->ipv4_neighbors, entry) {
		if (neigh->ip.v4.s_addr == ip.s_addr)
			break;
	}
	if (!neigh)
		goto not_found;

	--i; tag = CONFD_GET_XMLTAG(&kp->v[i][0]);
	switch (tag) {
	case ip_ip:
		CONFD_SET_IPV4(&v, ip);
		break;
	case ip_link_layer_address:
		CONFD_SET_BINARY(&v, neigh->lladdr, IFHWADDRLEN);
		break;
	case ip_origin:
		if (neigh->state & NUD_PERMANENT)
			CONFD_SET_ENUM_VALUE(&v, ip_static);
		else
			CONFD_SET_ENUM_VALUE(&v, ip_dynamic);
		break;
	default:
		goto not_found;
	}
	confd_data_reply_value(tctx, &v);
	return CONFD_OK;

not_found:
	confd_data_reply_not_found(tctx);
	return CONFD_OK;
}

int
ipv4_get_case(struct confd_trans_ctx *tctx, confd_hkeypath_t *kp,
    confd_value_t *choice)
{
	confd_value_t	v;

	CONFD_SET_XMLTAG(&v, ip_prefix_length, ip__ns);
	confd_data_reply_value(tctx, &v);
	return CONFD_OK;
}

int
ipv6_get_next(struct confd_trans_ctx *tctx, confd_hkeypath_t *kp, long next)
{
	struct iface	*iface;
	int		 tag, i = kp->len;
	char		*ifname;
	unsigned int	 ifindex;

	--i; tag = CONFD_GET_XMLTAG(&kp->v[i][0]);
	if (tag != if_interfaces)
		goto not_found;

	--i; tag = CONFD_GET_XMLTAG(&kp->v[i][0]);
	if (tag != if_interface)
		goto not_found;

	--i; ifname = CONFD_GET_CBUFPTR(&kp->v[i][0]);
	ifindex = if_nametoindex(ifname);
	if (!ifindex)
		goto not_found;

	iface = if_find(ifindex);
	if (!iface)
		goto not_found;

	--i; tag = CONFD_GET_XMLTAG(&kp->v[i][0]);
	if (tag != ip_ipv6)
		goto not_found;

	--i; tag = CONFD_GET_XMLTAG(&kp->v[i][0]);
	switch (tag) {
	case ip_address:
		return ipv6_get_next_address(tctx, next, iface);
	case ip_neighbor:
		return ipv6_get_next_neighbor(tctx, next, iface);
	}

not_found:
	confd_data_reply_next_key(tctx, NULL, 0, 0);
	return CONFD_OK;
}

int
ipv6_get_elem(struct confd_trans_ctx *tctx, confd_hkeypath_t *kp)
{
	struct iface		*iface;
	confd_value_t		 v;
	int			 val, tag, i = kp->len;
	char			*ifname;
	unsigned int		 ifindex;

	--i; tag = CONFD_GET_XMLTAG(&kp->v[i][0]);
	if (tag != if_interfaces)
		goto not_found;

	--i; tag = CONFD_GET_XMLTAG(&kp->v[i][0]);
	if (tag != if_interface)
		goto not_found;

	--i; ifname = CONFD_GET_CBUFPTR(&kp->v[i][0]);
	ifindex = if_nametoindex(ifname);
	if (!ifindex)
		goto not_found;

	iface = if_find(ifindex);
	if (!iface)
		goto not_found;

	--i; tag = CONFD_GET_XMLTAG(&kp->v[i][0]);
	if (tag != ip_ipv6)
		goto not_found;

	if (i < 1) {	/* ipv6 presence container */
		if (iface->flags & IFF_UP)
			confd_data_reply_found(tctx);
		else
			confd_data_reply_not_found(tctx);
		return CONFD_OK;
	}

	--i; tag = CONFD_GET_XMLTAG(&kp->v[i][0]);
	switch (tag) {
	case ip_mtu:
		val = proc_get("/proc/sys/net/ipv6/conf/%s/mtu", ifname);
		CONFD_SET_UINT32(&v, val);
		break;
	case ip_forwarding:
		val = proc_get("/proc/sys/net/ipv6/conf/%s/forwarding",
		    ifname);
		CONFD_SET_BOOL(&v, val);
		break;
	case ip_address:
		return ipv6_get_elem_address(tctx, kp, iface);
	case ip_neighbor:
		return ipv6_get_elem_neighbor(tctx, kp, iface);
	case v6ur_ipv6_router_advertisements:
		return ipv6_get_elem_radv(tctx, kp, iface);
	default:
		goto not_found;
	}
	confd_data_reply_value(tctx, &v);
	return CONFD_OK;

not_found:
	confd_data_reply_not_found(tctx);
	return CONFD_OK;
}

int
ipv6_get_elem_address(struct confd_trans_ctx *tctx, confd_hkeypath_t *kp,
    struct iface *iface)
{
	struct addr	*addr;
	struct in6_addr	 ip;
	confd_value_t	 v;
	int		 tag, i = kp->len - 5;

	--i; ip = CONFD_GET_IPV6(&kp->v[i][0]);
	TAILQ_FOREACH(addr, &iface->ipv6_addrs, entry) {
		if (!memcmp(&addr->ip.v6, &ip, sizeof ip))
			break;
	}
	if (!addr)
		goto not_found;

	--i; tag = CONFD_GET_XMLTAG(&kp->v[i][0]);
	switch (tag) {
	case ip_ip:
		CONFD_SET_IPV6(&v, ip);
		break;
	case ip_prefix_length:
		CONFD_SET_UINT8(&v, addr->prefixlen);
		break;
	case ip_origin:
		CONFD_SET_ENUM_VALUE(&v, addr_origin(addr));
		break;
	case ip_status:
		CONFD_SET_ENUM_VALUE(&v, addr_status(addr));
		break;
	default:
		goto not_found;
	}
	confd_data_reply_value(tctx, &v);
	return CONFD_OK;

not_found:
	confd_data_reply_not_found(tctx);
	return CONFD_OK;
}

int
ipv6_get_elem_neighbor(struct confd_trans_ctx *tctx, confd_hkeypath_t *kp,
    struct iface *iface)
{
	struct neighbor	*neigh;
	struct in6_addr	 ip;
	confd_value_t	 v;
	int		 tag, i = kp->len - 5;

	--i; ip = CONFD_GET_IPV6(&kp->v[i][0]);
	TAILQ_FOREACH(neigh, &iface->ipv6_neighbors, entry) {
		if (!memcmp(&neigh->ip.v6, &ip, sizeof ip))
			break;
	}
	if (!neigh)
		goto not_found;

	--i; tag = CONFD_GET_XMLTAG(&kp->v[i][0]);
	switch (tag) {
	case ip_ip:
		CONFD_SET_IPV6(&v, ip);
		break;
	case ip_link_layer_address:
		CONFD_SET_BINARY(&v, neigh->lladdr, IFHWADDRLEN);
		break;
	case ip_origin:
		if (neigh->state & (NUD_PERMANENT | NUD_NOARP))
			CONFD_SET_ENUM_VALUE(&v, ip_static);
		else
			CONFD_SET_ENUM_VALUE(&v, ip_dynamic);
		break;
	case ip_is_router:
		if (neigh->flags & NTF_ROUTER)
			confd_data_reply_found(tctx);
		else
			confd_data_reply_not_found(tctx);
		return CONFD_OK;
	case ip_state:
		CONFD_SET_ENUM_VALUE(&v, neighbor_state(neigh->state));
		break;
	default:
		goto not_found;
	}
	confd_data_reply_value(tctx, &v);
	return CONFD_OK;

not_found:
	confd_data_reply_not_found(tctx);
	return CONFD_OK;
}

int
ipv6_get_elem_radv(struct confd_trans_ctx *tctx, confd_hkeypath_t *kp,
    struct iface *iface)
{
	confd_value_t	 v;
	int		 val, tag, i = kp->len - 5;

	--i; tag = CONFD_GET_XMLTAG(&kp->v[i][0]);
	switch (tag) {
	case v6ur_retrans_timer:
		val = proc_get("/proc/sys/net/ipv6/neigh/%s/"
		    "retrans_time_ms", iface->name);
		CONFD_SET_UINT32(&v, val);
		break;
	case v6ur_reachable_time:
		val = proc_get("/proc/sys/net/ipv6/neigh/%s/"
		    "base_reachable_time_ms", iface->name);
		CONFD_SET_UINT32(&v, val);
		break;
	case v6ur_cur_hop_limit:
		val = proc_get("/proc/sys/net/ipv6/conf/%s/"
		    "hop_limit", iface->name);
		CONFD_SET_UINT8(&v, val);
		break;
	case v6ur_link_mtu:
		val = proc_get("/proc/sys/net/ipv6/conf/%s/"
		    "mtu", iface->name);
		CONFD_SET_UINT32(&v, val);
		break;
	default:
		goto not_found;
	}
	confd_data_reply_value(tctx, &v);
	return CONFD_OK;

not_found:
	confd_data_reply_not_found(tctx);
	return CONFD_OK;
}
int
addr_origin(struct addr *addr)
{
	if (addr->flags & IFA_F_TEMPORARY)
		return ip_random;

	switch (addr->family) {
	case AF_INET:
		/* 169.254.x.x */
		if (ntohl(addr->ip.v4.s_addr) >> 16 == 0xa9fe)
			return ip_random;
		else
			return ip_static;
	case AF_INET6:
		if (IN6_IS_ADDR_LINKLOCAL(&addr->ip.v6))
			return ip_link_layer;
		else
			return ip_static;
	}

	return ip_other;
}

int
addr_status(struct addr *addr)
{
	switch (addr->flags) {
	case IFA_F_DEPRECATED:
		return ip_deprecated;
	case IFA_F_OPTIMISTIC:
		return ip_optimistic;
	case IFA_F_PERMANENT:
		return ip_preferred;
	case IFA_F_TENTATIVE:
		return ip_tentative;
	case IFA_F_DADFAILED:
		return ip_duplicate;
	default:
		return ip_unknown;
	}
}

int
neighbor_state(int state)
{
	switch (state) {
	case NUD_INCOMPLETE:
		return ip_incomplete;
	case NUD_REACHABLE:
		return ip_reachable;
	case NUD_STALE:
		return ip_stale;
	case NUD_DELAY:
		return ip_delay;
	case NUD_PROBE:
		return ip_probe;
	default:
		return 0;
	}
}

int
oper_state(int state)
{
	switch (state) {
	case IF_OPER_NOTPRESENT:
		return if_not_present;
	case IF_OPER_DOWN:
		return if_down;
	case IF_OPER_LOWERLAYERDOWN:
		return if_lower_layer_down;
	case IF_OPER_TESTING:
		return if_testing;
	case IF_OPER_DORMANT:
		return if_dormant;
	case IF_OPER_UP:
		return if_up;
	case IF_OPER_UNKNOWN:
		/* FALLTHROUGH */
	default:
		return if_unknown;
	}
}

int
proc_get(const char *fmt, ...)
{
	FILE		*file;
	char		 path[PATH_MAX];
	va_list		 ap;
	int		 val;

	va_start(ap, fmt);
	vsnprintf(path, sizeof path, fmt, ap);
	va_end(ap);

	file = fopen(path, "r");
	if (!file)
		fatal("proc_get: fopen %s", path);

	if (fscanf(file, "%d", &val) != 1)
		fatal("proc_get: fscanf");

	fclose(file);
	return val;
}

void
proc_set(int val, const char *fmt, ...)
{
	FILE		*file;
	char		 path[PATH_MAX];
	va_list		 ap;

	va_start(ap, fmt);
	vsnprintf(path, sizeof path, fmt, ap);
	va_end(ap);

	file = fopen(path, "w");
	if (!file)
		fatal("proc_set: fopen %s", path);

	if (fprintf(file, "%d", val) < 0)
		fatal("proc_set: fprintf");

	fclose(file);
}
