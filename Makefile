CFLAGS+= -Wall -Werror -Wstrict-prototypes
CFLAGS+= -Wmissing-prototypes -Wmissing-declarations
CFLAGS+= -Wshadow -Wpointer-arith -Wcast-qual -Wsign-compare
CFLAGS+= -D_GNU_SOURCE
CFLAGS+= $(shell pkg-config --cflags libevent)
LDLIBS+= $(shell pkg-config --libs libevent)
CFLAGS+= $(shell pkg-config --cflags libconfd)
LDLIBS+= $(shell pkg-config --libs libconfd)
CFLAGS+= $(shell pkg-config --cflags libbsd-overlay)
LDLIBS+= $(shell pkg-config --libs libbsd)
LDLIBS+= -lpthread

CONFDC?= confdc
SMIDUMP?= smidump

bindir?= /usr/bin

SRC=	linkd.c log.c system.c base64.c netlink.c interfaces.c routing.c
OBJ=	$(SRC:%.c=%.o)
DEP=	$(SRC:%.c=%.d)
PRG=	linkd
YNG=	ietf-system.yang \
	ietf-interfaces.yang \
	iana-if-type.yang \
	ietf-ip.yang \
	ietf-routing.yang \
	ietf-ipv4-unicast-routing.yang \
	ietf-ipv6-unicast-routing.yang
FXS=	$(YNG:.yang=.fxs)
GEN=	$(YNG:.yang=.h)

.PRECIOUS: $(GEN)

.PHONY: all install clean

all:	$(PRG) $(FXS)

$(PRG):	$(OBJ)

%.d: %.c | $(GEN)
	$(CPP) $(CPPFLAGS) -c -MM -MT $@ -MT $*.o -MF $@ $<

iana-if-type.fxs: iana-if-type.yang
	$(CONFDC) -c -o $@ $<

%.fxs: %.yang %-ann.yang
	$(CONFDC) -c -o $@ -a $*-ann.yang $<

ietf-ipv6-unicast-routing.fxs: ietf-ipv6-router-advertisements.yang

%.h: %.fxs
	$(CONFDC) --emit-h $@ $<

%.ccl: %.cli
	$(CONFDC) -c -o $@ $<

install: $(PRG) $(FXS)
	install -m 0755 -d $(DESTDIR)$(bindir)
	install -m 0755 $(PRG) $(DESTDIR)$(bindir)
	install -m 0755 -d $(DESTDIR)/etc/confd
	install -m 0644 $(FXS) $(DESTDIR)/etc/confd

clean:
	@rm -f $(PRG) $(OBJ) $(DEP) $(FXS) $(GEN)

ifneq ($(MAKECMDGOALS),clean)
-include $(DEP)
endif
