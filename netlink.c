/*
 * Copyright (c) 2017 Ruslan Babayev <ruslan@babayev.com>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <net/if_arp.h>
#include <net/if.h>
#include <linux/if.h>
#include <linux/netlink.h>
#include <linux/rtnetlink.h>

#include <errno.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>

#include "netlink.h"
#include "log.h"

int	 dispatch_nlmsg(struct nlmsghdr *);
void	 rtattr_parse_nested(struct rtattr *, struct rtattr **, int);
int	 rtattr_add(struct nlmsghdr *, size_t, int, void *, size_t);
struct rtattr
	*rtattr_nested_add(struct nlmsghdr *, size_t, int);
void	 rtattr_nested_end(struct nlmsghdr *, struct rtattr *);

int	 handle_newlink(struct nlmsghdr *);
int	 handle_dellink(struct nlmsghdr *);
int	 handle_newaddr(struct nlmsghdr *);
int	 handle_deladdr(struct nlmsghdr *);
int	 handle_newneigh(struct nlmsghdr *);
int	 handle_delneigh(struct nlmsghdr *);
int	 handle_newroute(struct nlmsghdr *);
int	 handle_delroute(struct nlmsghdr *);

static uint32_t	 seq;

int
nl_socket(void)
{
	int	sock, rv, saved_errno;
	int	bufsz = 10 * BUFSIZ;

	sock = socket(PF_NETLINK, SOCK_RAW, NETLINK_ROUTE);
	if (sock < 0)
		return sock;

	rv = setsockopt(sock, SOL_SOCKET, SO_RCVBUFFORCE, &bufsz,
	    sizeof bufsz);
	if (rv < 0)
		goto out;

	rv = fcntl(sock, F_SETFL, O_NONBLOCK);
	if (rv < 0)
		goto out;

	return sock;
out:
	saved_errno = errno;
	close(sock);
	errno = saved_errno;
	return rv;
}

int
nl_bind(int sock)
{
	struct sockaddr_nl	 snl;

	memset(&snl, 0, sizeof snl);
	snl.nl_family = AF_NETLINK;
	snl.nl_groups = RTMGRP_LINK | RTMGRP_NEIGH |
	    RTMGRP_IPV4_IFADDR | RTMGRP_IPV6_IFADDR |
	    RTMGRP_IPV4_ROUTE | RTMGRP_IPV6_ROUTE;

	return bind(sock, (struct sockaddr *)&snl, sizeof snl);
}

int
getlink(int ifindex)
{
	struct sockaddr_nl		snl;
	struct {
		struct nlmsghdr		nlmsg;
		struct ifinfomsg	ifi;
	} req = {
		.nlmsg.nlmsg_len	= NLMSG_LENGTH(sizeof(struct ifinfomsg)),
		.nlmsg.nlmsg_flags	= NLM_F_REQUEST,
		.nlmsg.nlmsg_type	= RTM_GETLINK,
		.nlmsg.nlmsg_pid	= getpid(),
		.nlmsg.nlmsg_seq	= ++seq,
		.ifi.ifi_family		= AF_UNSPEC,
		.ifi.ifi_index		= ifindex,
	};
	int	sock, rv, saved_errno;

	log_debug("getlink: ifindex:%d", ifindex);

	if ((sock = nl_socket()) < 0)
		return (sock);

	if (!ifindex)
		req.nlmsg.nlmsg_flags |= NLM_F_DUMP;

	bzero(&snl, sizeof(snl));
	snl.nl_family = AF_NETLINK;

	rv = sendto(sock, &req, sizeof req, 0, (struct sockaddr *)&snl,
	    sizeof snl);
	if (rv < 0)
		goto out;

	rv = recv_nlmsg(sock);
out:
	saved_errno = errno;
	close(sock);
	errno = saved_errno;
	return (rv);
}

int
getaddr(int ifindex, int family)
{
	struct sockaddr_nl		snl;
	struct {
		struct nlmsghdr		nlmsg;
		struct ifaddrmsg	ifa;
	} req = {
		.nlmsg.nlmsg_len	= NLMSG_LENGTH(sizeof(struct ifaddrmsg)),
		.nlmsg.nlmsg_flags	= NLM_F_REQUEST,
		.nlmsg.nlmsg_type	= RTM_GETADDR,
		.nlmsg.nlmsg_pid	= getpid(),
		.nlmsg.nlmsg_seq	= ++seq,
		.ifa.ifa_family		= family,
		.ifa.ifa_index		= ifindex,
	};
	int	sock, rv, saved_errno;

	log_debug("getaddr: ifindex:%d family:%d", ifindex, family);

	if ((sock = nl_socket()) < 0)
		return (sock);

	if (!ifindex)
		req.nlmsg.nlmsg_flags |= NLM_F_DUMP;

	bzero(&snl, sizeof(snl));
	snl.nl_family = AF_NETLINK;

	rv = sendto(sock, &req, sizeof req, 0, (struct sockaddr *)&snl,
	    sizeof snl);
	if (rv < 0)
		goto out;

	rv = recv_nlmsg(sock);
out:
	saved_errno = errno;
	close(sock);
	errno = saved_errno;
	return (rv);
}

int
newaddr(int ifindex, int family, void *addr, uint8_t prefixlen)
{
	struct sockaddr_nl		snl;
	struct {
		struct nlmsghdr		nlmsg;
		struct ifaddrmsg	ifa;
		char			buf[256];
	} req = {
		.nlmsg.nlmsg_len	= NLMSG_LENGTH(sizeof(struct ifaddrmsg)),
		.nlmsg.nlmsg_flags	= NLM_F_REQUEST | NLM_F_ACK |
					  NLM_F_CREATE | NLM_F_REPLACE,
		.nlmsg.nlmsg_type	= RTM_NEWADDR,
		.nlmsg.nlmsg_pid	= getpid(),
		.nlmsg.nlmsg_seq	= ++seq,
		.ifa.ifa_family		= family,
		.ifa.ifa_index		= ifindex,
		.ifa.ifa_prefixlen	= prefixlen,
	};
	int	sock, rv, saved_errno, maxlen = sizeof req;
	char	str[256];

	log_debug("newaddr: %s/%d", inet_ntop(family, addr, str, sizeof str),
	    prefixlen);

	if ((sock = nl_socket()) < 0)
		return (sock);

	rv = rtattr_add(&req.nlmsg, maxlen, IFA_LOCAL, addr,
	    family == AF_INET ? 4 : 16);
	if (rv < 0)
		goto out;

	rv = rtattr_add(&req.nlmsg, maxlen, IFA_ADDRESS, addr,
	    family == AF_INET ? 4 : 16);
	if (rv < 0)
		goto out;

	bzero(&snl, sizeof(snl));
	snl.nl_family = AF_NETLINK;

	rv = sendto(sock, &req, sizeof req, 0, (struct sockaddr *)&snl,
	    sizeof snl);
	if (rv < 0)
		goto out;

	rv = recv_nlmsg(sock);
out:
	saved_errno = errno;
	close(sock);
	errno = saved_errno;
	return (rv);
}

int
deladdr(int ifindex, int family, void *addr, uint8_t prefixlen)
{
	struct sockaddr_nl		snl;
	struct {
		struct nlmsghdr		nlmsg;
		struct ifaddrmsg	ifa;
		char			buf[256];
	} req = {
		.nlmsg.nlmsg_len	= NLMSG_LENGTH(sizeof(struct ifaddrmsg)),
		.nlmsg.nlmsg_flags	= NLM_F_REQUEST | NLM_F_ACK,
		.nlmsg.nlmsg_type	= RTM_DELADDR,
		.nlmsg.nlmsg_pid	= getpid(),
		.nlmsg.nlmsg_seq	= ++seq,
		.ifa.ifa_family		= family,
		.ifa.ifa_index		= ifindex,
		.ifa.ifa_prefixlen	= prefixlen,
	};
	int	sock, rv, saved_errno, maxlen = sizeof req;
	char	str[256];

	if ((sock = nl_socket()) < 0)
		return (sock);

	log_debug("deladdr: %s/%d", inet_ntop(family, addr, str, sizeof str),
	    prefixlen);

	rv = rtattr_add(&req.nlmsg, maxlen, IFA_LOCAL, addr,
	    family == AF_INET ? 4 : 16);
	if (rv < 0)
		goto out;

	rv = rtattr_add(&req.nlmsg, maxlen, IFA_ADDRESS, addr,
	    family == AF_INET ? 4 : 16);
	if (rv < 0)
		goto out;

	bzero(&snl, sizeof(snl));
	snl.nl_family = AF_NETLINK;

	rv = sendto(sock, &req, sizeof req, 0, (struct sockaddr *)&snl,
	    sizeof snl);
	if (rv < 0)
		goto out;

	rv = recv_nlmsg(sock);
out:
	saved_errno = errno;
	close(sock);
	errno = saved_errno;
	return (rv);
}

int
getneigh(int ifindex, int family)
{
	struct sockaddr_nl		snl;
	struct {
		struct nlmsghdr		nlmsg;
		struct ndmsg		ndm;
	} req = {
		.nlmsg.nlmsg_len	= NLMSG_LENGTH(sizeof(struct ndmsg)),
		.nlmsg.nlmsg_flags	= NLM_F_REQUEST,
		.nlmsg.nlmsg_type	= RTM_GETNEIGH,
		.nlmsg.nlmsg_pid	= getpid(),
		.nlmsg.nlmsg_seq	= ++seq,
		.ndm.ndm_family		= family,
		.ndm.ndm_ifindex	= ifindex,
	};
	int	sock, rv, saved_errno;

	log_debug("getneigh: ifindex:%d family:%d", ifindex, family);

	if ((sock = nl_socket()) < 0)
		return (sock);

	if (!ifindex)
		req.nlmsg.nlmsg_flags |= NLM_F_DUMP;

	bzero(&snl, sizeof(snl));
	snl.nl_family = AF_NETLINK;

	rv = sendto(sock, &req, sizeof req, 0, (struct sockaddr *)&snl,
	    sizeof snl);
	if (rv < 0)
		goto out;

	rv = recv_nlmsg(sock);
out:
	saved_errno = errno;
	close(sock);
	errno = saved_errno;
	return (rv);
}

int
newneigh(int ifindex, int family, void *dst, void *lladdr)
{
	struct sockaddr_nl		snl;
	struct {
		struct nlmsghdr		nlmsg;
		struct ndmsg		ndm;
		char			buf[256];
	} req = {
		.nlmsg.nlmsg_len	= NLMSG_LENGTH(sizeof(struct ndmsg)),
		.nlmsg.nlmsg_flags	= NLM_F_REQUEST | NLM_F_ACK |
					  NLM_F_CREATE | NLM_F_REPLACE,
		.nlmsg.nlmsg_type	= RTM_NEWNEIGH,
		.nlmsg.nlmsg_pid	= getpid(),
		.nlmsg.nlmsg_seq	= ++seq,
		.ndm.ndm_family		= family,
		.ndm.ndm_ifindex	= ifindex,
		.ndm.ndm_state		= NUD_PERMANENT,
	};
	int	sock, rv, saved_errno, maxlen = sizeof req;
	char	str[256];

	log_debug("newneigh: %s", inet_ntop(family, dst, str, sizeof str));

	if ((sock = nl_socket()) < 0)
		return (sock);

	rv = rtattr_add(&req.nlmsg, maxlen, NDA_DST, dst,
	    family == AF_INET ? 4 : 16);
	if (rv < 0)
		goto out;

	rv = rtattr_add(&req.nlmsg, maxlen, NDA_LLADDR, lladdr, IFHWADDRLEN);
	if (rv < 0)
		goto out;

	bzero(&snl, sizeof(snl));
	snl.nl_family = AF_NETLINK;

	rv = sendto(sock, &req, sizeof req, 0, (struct sockaddr *)&snl,
	    sizeof snl);
	if (rv < 0)
		goto out;

	rv = recv_nlmsg(sock);
out:
	saved_errno = errno;
	close(sock);
	errno = saved_errno;
	return (rv);
}

int
delneigh(int ifindex, int af, void *dst, void *lladdr)
{
	struct sockaddr_nl		snl;
	struct {
		struct nlmsghdr		nlmsg;
		struct ndmsg		ndm;
		char			buf[256];
	} req = {
		.nlmsg.nlmsg_len	= NLMSG_LENGTH(sizeof(struct ndmsg)),
		.nlmsg.nlmsg_flags	= NLM_F_REQUEST | NLM_F_ACK,
		.nlmsg.nlmsg_type	= RTM_DELNEIGH,
		.nlmsg.nlmsg_pid	= getpid(),
		.nlmsg.nlmsg_seq	= ++seq,
		.ndm.ndm_family		= af,
		.ndm.ndm_ifindex	= ifindex,
		.ndm.ndm_state		= NUD_PERMANENT,
	};
	int	sock, rv, saved_errno, maxlen = sizeof req;
	char	str[256];

	log_debug("delneigh: %s", inet_ntop(af, dst, str, sizeof str));

	if ((sock = nl_socket()) < 0)
		return (sock);

	rv = rtattr_add(&req.nlmsg, maxlen, NDA_DST, dst,
	    af == AF_INET ? 4 : 16);
	if (rv < 0)
		goto out;

	if (lladdr) {
		rv = rtattr_add(&req.nlmsg, maxlen, NDA_LLADDR, lladdr,
		    IFHWADDRLEN);
		if (rv < 0)
			goto out;
	}

	bzero(&snl, sizeof(snl));
	snl.nl_family = AF_NETLINK;

	rv = sendto(sock, &req, sizeof req, 0, (struct sockaddr *)&snl,
	    sizeof snl);
	if (rv < 0)
		goto out;

	rv = recv_nlmsg(sock);
out:
	saved_errno = errno;
	close(sock);
	errno = saved_errno;
	return (rv);
}

int
getroute(int table, int family)
{
	struct sockaddr_nl		snl;
	struct {
		struct nlmsghdr		nlmsg;
		struct rtmsg		rtm;
	} req = {
		.nlmsg.nlmsg_len	= NLMSG_LENGTH(sizeof(struct rtmsg)),
		.nlmsg.nlmsg_flags	= NLM_F_REQUEST|NLM_F_DUMP,
		.nlmsg.nlmsg_type	= RTM_GETROUTE,
		.nlmsg.nlmsg_pid	= getpid(),
		.rtm.rtm_table		= table,
		.rtm.rtm_family		= family,
	};
	int	sock, rv, saved_errno;

	log_debug("getroute: table:%d family:%d", table, family);

	if ((sock = nl_socket()) < 0)
		return (sock);

	bzero(&snl, sizeof(snl));
	snl.nl_family = AF_NETLINK;

	rv = sendto(sock, &req, sizeof req, 0, (struct sockaddr *)&snl,
	    sizeof snl);
	if (rv < 0)
		goto out;

	rv = recv_nlmsg(sock);
out:
	saved_errno = errno;
	close(sock);
	errno = saved_errno;
	return (rv);
}

int
newroute(int table, int family, int type, void *prefix, uint8_t prefixlen,
    void *nh, int ifindex)
{
	struct sockaddr_nl		snl;
	struct {
		struct nlmsghdr		nlmsg;
		struct rtmsg		rtm;
		char			buf[512];
	} req = {
		.nlmsg.nlmsg_len	= NLMSG_LENGTH(sizeof(struct rtmsg)),
		.nlmsg.nlmsg_flags	= NLM_F_REQUEST | NLM_F_ACK |
					  NLM_F_CREATE | NLM_F_REPLACE,
		.nlmsg.nlmsg_type	= RTM_NEWROUTE,
		.nlmsg.nlmsg_pid	= getpid(),
		.nlmsg.nlmsg_seq	= ++seq,
		.rtm.rtm_family		= family,
		.rtm.rtm_table		= table,
		.rtm.rtm_type		= type,
		.rtm.rtm_protocol	= RTPROT_STATIC,
		.rtm.rtm_dst_len	= prefixlen,
	};
	int	sock, rv, saved_errno, maxlen = sizeof req;
	char	str[256];

	log_debug("newroute: %s/%d type:%d",
	    inet_ntop(family, prefix, str, sizeof str),
	    prefixlen, type);

	if ((sock = nl_socket()) < 0)
		return (sock);

	rv = rtattr_add(&req.nlmsg, maxlen, RTA_DST, prefix,
	    family == AF_INET ? 4 : 16);
	if (rv < 0)
		goto out;

	if (nh && type == RTN_UNICAST) {
		rv = rtattr_add(&req.nlmsg, maxlen, RTA_GATEWAY, nh,
		    family == AF_INET ? 4 : 16);
		if (rv < 0)
			goto out;
	}

	if (ifindex) {
		rv = rtattr_add(&req.nlmsg, maxlen, RTA_OIF, &ifindex, 4);
		if (rv < 0)
			goto out;
	}

	bzero(&snl, sizeof(snl));
	snl.nl_family = AF_NETLINK;

	rv = sendto(sock, &req, sizeof req, 0, (struct sockaddr *)&snl,
	    sizeof snl);
	if (rv < 0)
		goto out;

	rv = recv_nlmsg(sock);
out:
	saved_errno = errno;
	close(sock);
	errno = saved_errno;
	return (rv);
}

int
delroute(int table, int family, void *prefix, uint8_t prefixlen)
{
	struct sockaddr_nl		snl;
	struct {
		struct nlmsghdr		nlmsg;
		struct rtmsg		rtm;
		char			buf[256];
	} req = {
		.nlmsg.nlmsg_len	= NLMSG_LENGTH(sizeof(struct rtmsg)),
		.nlmsg.nlmsg_flags	= NLM_F_REQUEST | NLM_F_ACK,
		.nlmsg.nlmsg_type	= RTM_DELROUTE,
		.nlmsg.nlmsg_pid	= getpid(),
		.nlmsg.nlmsg_seq	= ++seq,
		.rtm.rtm_family		= family,
		.rtm.rtm_table		= table,
		.rtm.rtm_dst_len	= prefixlen,
	};
	int	sock, rv, saved_errno, maxlen = sizeof req;
	char	str[256];

	if ((sock = nl_socket()) < 0)
		return (sock);

	log_debug("delroute: %s/%d",
	    inet_ntop(family, prefix, str, sizeof str),
	    prefixlen);

	rv = rtattr_add(&req.nlmsg, maxlen, RTA_DST, prefix,
	    family == AF_INET ? 4 : 16);
	if (rv < 0)
		goto out;

	bzero(&snl, sizeof(snl));
	snl.nl_family = AF_NETLINK;

	rv = sendto(sock, &req, sizeof req, 0, (struct sockaddr *)&snl,
	    sizeof snl);
	if (rv < 0)
		goto out;

	rv = recv_nlmsg(sock);
out:
	saved_errno = errno;
	close(sock);
	errno = saved_errno;
	return (rv);
}

int
link_set_flags(int ifindex, int flags)
{
	struct sockaddr_nl		snl;
	struct {
		struct nlmsghdr		nlmsg;
		struct ifinfomsg	ifi;
		char			buf[1024];
	} req = {
		.nlmsg.nlmsg_len	= NLMSG_LENGTH(sizeof(struct ifinfomsg)),
		.nlmsg.nlmsg_flags	= NLM_F_REQUEST | NLM_F_ACK,
		.nlmsg.nlmsg_type	= RTM_NEWLINK,
		.nlmsg.nlmsg_pid	= getpid(),
		.nlmsg.nlmsg_seq	= ++seq,
		.ifi.ifi_family		= AF_UNSPEC,
		.ifi.ifi_index		= ifindex,
		.ifi.ifi_change		= IFF_UP,
		.ifi.ifi_flags		= flags,
	};
	int	sock, rv, saved_errno;

	if ((sock = nl_socket()) < 0)
		return (sock);

	bzero(&snl, sizeof snl);
	snl.nl_family = AF_NETLINK;

	rv = sendto(sock, &req, sizeof req, 0, (struct sockaddr *)&snl,
	    sizeof snl);
	if (rv < 0)
		goto out;

	rv = recv_nlmsg(sock);
out:
	saved_errno = errno;
	close(sock);
	errno = saved_errno;
	return (rv);
}

int
link_set_mtu(int ifindex, int mtu)
{
	struct sockaddr_nl		snl;
	struct {
		struct nlmsghdr		nlmsg;
		struct ifinfomsg	ifi;
		char			buf[256];
	} req = {
		.nlmsg.nlmsg_len	= NLMSG_LENGTH(sizeof(struct ifinfomsg)),
		.nlmsg.nlmsg_flags	= NLM_F_REQUEST | NLM_F_ACK,
		.nlmsg.nlmsg_type	= RTM_NEWLINK,
		.nlmsg.nlmsg_pid	= getpid(),
		.nlmsg.nlmsg_seq	= ++seq,
		.ifi.ifi_family		= AF_INET,
		.ifi.ifi_index		= ifindex,
	};
	int	sock, rv, saved_errno, maxlen = sizeof req;

	if ((sock = nl_socket()) < 0)
		return (sock);

	rv = rtattr_add(&req.nlmsg, maxlen, IFLA_MTU, &mtu, 4);
	if (rv < 0)
		goto out;

	bzero(&snl, sizeof(snl));
	snl.nl_family = AF_NETLINK;

	rv = sendto(sock, &req, sizeof req, 0, (struct sockaddr *)&snl,
	    sizeof snl);
	if (rv < 0)
		goto out;

	rv = recv_nlmsg(sock);
out:
	saved_errno = errno;
	close(sock);
	errno = saved_errno;
	return (rv);
}

int
link_set_master(int ifindex, int master)
{
	struct sockaddr_nl		snl;
	struct {
		struct nlmsghdr		nlmsg;
		struct ifinfomsg	ifi;
		char			buf[1024];
	} req = {
		.nlmsg.nlmsg_len	= NLMSG_LENGTH(sizeof(struct ifinfomsg)),
		.nlmsg.nlmsg_flags	= NLM_F_REQUEST | NLM_F_ACK,
		.nlmsg.nlmsg_type	= RTM_NEWLINK,
		.nlmsg.nlmsg_pid	= getpid(),
		.nlmsg.nlmsg_seq	= ++seq,
		.ifi.ifi_family		= AF_UNSPEC,
		.ifi.ifi_index		= ifindex,
	};
	int	sock, rv, saved_errno, maxlen = sizeof req;

	if ((sock = nl_socket()) < 0)
		return (sock);

	rv = rtattr_add(&req.nlmsg, maxlen, IFLA_LINK, &ifindex, 4);
	if (rv < 0)
		goto out;

	rv = rtattr_add(&req.nlmsg, maxlen, IFLA_MASTER, &master, 4);
	if (rv < 0)
		goto out;

	bzero(&snl, sizeof snl);
	snl.nl_family = AF_NETLINK;

	rv = sendto(sock, &req, sizeof req, 0, (struct sockaddr *)&snl,
	    sizeof snl);
	if (rv < 0)
		goto out;

	rv = recv_nlmsg(sock);
out:
	saved_errno = errno;
	close(sock);
	errno = saved_errno;
	return (rv);
}

int
link_add_vrf(char *name, int table)
{
	struct sockaddr_nl		snl;
	struct {
		struct nlmsghdr		nlmsg;
		struct ifinfomsg	ifi;
		char			buf[1024];
	} req = {
		.nlmsg.nlmsg_len	= NLMSG_LENGTH(sizeof(struct ifinfomsg)),
		.nlmsg.nlmsg_flags	= NLM_F_REQUEST | NLM_F_ACK |
					  NLM_F_CREATE | NLM_F_REPLACE,
		.nlmsg.nlmsg_type	= RTM_NEWLINK,
		.nlmsg.nlmsg_pid	= getpid(),
		.nlmsg.nlmsg_seq	= ++seq,
		.ifi.ifi_family		= AF_UNSPEC,
		.ifi.ifi_index		= 0,
	};
	struct rtattr	*link_info, *info_data;
	int		 sock, rv, saved_errno, maxlen;

	if ((sock = nl_socket()) < 0)
		return (sock);

	maxlen = sizeof(req);
	rv = rtattr_add(&req.nlmsg, maxlen, IFLA_IFNAME,
	    name, strlen(name) + 1);
	if (rv < 0)
		goto out;

	link_info = rtattr_nested_add(&req.nlmsg, maxlen, IFLA_LINKINFO);
	if (!link_info)
		goto out;

	rv = rtattr_add(&req.nlmsg, maxlen, IFLA_INFO_KIND, "vrf", 4);
	if (rv < 0)
		goto out;

	info_data = rtattr_nested_add(&req.nlmsg, maxlen, IFLA_INFO_DATA);
	if (!info_data)
		goto out;

	rv = rtattr_add(&req.nlmsg, maxlen, IFLA_VRF_TABLE, &table, 4);
	if (rv < 0)
		goto out;

	rtattr_nested_end(&req.nlmsg, info_data);
	rtattr_nested_end(&req.nlmsg, link_info);

	bzero(&snl, sizeof(snl));
	snl.nl_family = AF_NETLINK;

	rv = sendto(sock, &req, sizeof req, 0, (struct sockaddr *)&snl,
	    sizeof snl);
	if (rv < 0)
		goto out;

	rv = recv_nlmsg(sock);
out:
	saved_errno = errno;
	close(sock);
	errno = saved_errno;
	return (rv);
}

int
recv_nlmsg(int sock)
{
	struct sockaddr_nl	 snl;
	struct iovec		 iov;
	struct msghdr		 msg;
	struct nlmsghdr		*nlmsg;
	struct nlmsgerr		*err;
	char			 buf[BUFSIZ];
	ssize_t			 n;
	int			 multipart = 0, interrupted = 0;

again:
	iov.iov_base = buf;
	iov.iov_len = sizeof(buf);

	memset(&snl, 0, sizeof(snl));
	snl.nl_family = AF_NETLINK;

	memset(&msg, 0, sizeof(msg));
	msg.msg_name = &snl;
	msg.msg_namelen = sizeof(snl);
	msg.msg_iov = &iov;
	msg.msg_iovlen = 1;

	n = recvmsg(sock, &msg, 0);
	if (n < 0) {
		if (errno == EINTR || errno == EAGAIN)
			goto again;
		log_warn("recv_nlmsg: recvmsg error");
		return (-1);
	}

	if (n == 0) {
		log_warnx("recv_nlmsg: netlink socket closed");
		return (-1);
	}

	if (msg.msg_flags & MSG_TRUNC) {
		log_warnx("recv_nlmsg: truncated message");
		return (-1);
	}

	if (msg.msg_namelen != sizeof(snl)) {
		log_warnx("recv_nlmsg: sockaddr_nl invalid length %d",
		    msg.msg_namelen);
		return (-1);
	}

	for (nlmsg = (struct nlmsghdr *)buf;
	     NLMSG_OK(nlmsg, n);
	     nlmsg = NLMSG_NEXT(nlmsg, n)) {
		log_debug("recv_nlmsg: seq:%u type:%u flags:0x%x",
		    nlmsg->nlmsg_seq, nlmsg->nlmsg_type, nlmsg->nlmsg_flags);

		if (nlmsg->nlmsg_flags & NLM_F_MULTI)
			multipart = 1;

		if (nlmsg->nlmsg_flags & NLM_F_DUMP_INTR)
			interrupted = 1;

		switch (nlmsg->nlmsg_type) {
		case NLMSG_DONE:
			multipart = 0;
			break;
		case NLMSG_NOOP:
			continue;
		case NLMSG_OVERRUN:
			log_warnx("recv_nlmsg: overrun");
			return (-1);
		case NLMSG_ERROR:
			if (nlmsg->nlmsg_len < NLMSG_SPACE(sizeof(*err))) {
				log_warnx("recv_nlmsg: truncated error");
				return (-1);
			}
			err = NLMSG_DATA(nlmsg);
			if (!err->error) {
				log_debug("recv_nlmsg: ack");
				continue;
			}
			errno = -err->error;
			return (-1);
		default:
			if (dispatch_nlmsg(nlmsg) < 0) {
				return (-1);
			}
		}
	}

	if (n) {
		log_warnx("recv_nlmsg: %ld bytes remaining", n);
		return (-1);
	}

	if (multipart)
		goto again;

	if (interrupted) {
		log_warnx("recv_nlmsg: interrupted");
		return (-1);
	}

	return (0);
}

int
dispatch_nlmsg(struct nlmsghdr *nlmsg)
{

	switch (nlmsg->nlmsg_type) {
	case RTM_NEWLINK:
		return handle_newlink(nlmsg);
	case RTM_DELLINK:
		return handle_dellink(nlmsg);
	case RTM_NEWADDR:
		return handle_newaddr(nlmsg);
	case RTM_DELADDR:
		return handle_deladdr(nlmsg);
	case RTM_NEWNEIGH:
		return handle_newneigh(nlmsg);
	case RTM_DELNEIGH:
		return handle_delneigh(nlmsg);
	case RTM_NEWROUTE:
		return handle_newroute(nlmsg);
	case RTM_DELROUTE:
		return handle_delroute(nlmsg);
	default:
		return (0);
	}
}

void
rtattr_parse(struct rtattr *rta, int len, struct rtattr *tb[], int max)
{
	memset(tb, 0, sizeof (struct rtattr *) * (max + 1));
	for (; RTA_OK(rta, len); rta = RTA_NEXT(rta, len)) {
		if (rta->rta_type <= max)
			tb[rta->rta_type] = rta;
	}
}

inline void
rtattr_parse_nested(struct rtattr *rta, struct rtattr *tb[], int max)
{
	rtattr_parse(RTA_DATA(rta), RTA_PAYLOAD(rta), tb, max);
}

int
rtattr_add(struct nlmsghdr *n, size_t maxlen, int type, void *data, size_t len)
{
	struct rtattr	*rta;
	size_t		 rlen;

	rlen = RTA_LENGTH(len);
	if (NLMSG_ALIGN(n->nlmsg_len) + rlen > maxlen)
		return (-1);

	rta = (struct rtattr *)((char *)n + NLMSG_ALIGN(n->nlmsg_len));
	rta->rta_type = type;
	rta->rta_len = rlen;
	memcpy(RTA_DATA(rta), data, len);
	n->nlmsg_len = NLMSG_ALIGN(n->nlmsg_len) + rlen;
	return (0);
}

struct rtattr *
rtattr_nested_add(struct nlmsghdr *n, size_t maxlen, int type)
{

	if (rtattr_add(n, maxlen, type, NULL, 0) < 0)
		return NULL;

	return (struct rtattr *)((char *)n + NLMSG_ALIGN(n->nlmsg_len));
}

void
rtattr_nested_end(struct nlmsghdr *n, struct rtattr *rta)
{
	rta->rta_len = (char *)n + NLMSG_ALIGN(n->nlmsg_len) - (char *)rta;
}

uint32_t
rtattr_get_u32(struct rtattr *rta)
{
	uint32_t	*data = RTA_DATA(rta);

	return rta ? *data : 0;
}

uint8_t
rtattr_get_u8(struct rtattr *rta)
{
	uint8_t		*data = RTA_DATA(rta);

	return rta ? *data : 0;
}
