/*
 * Copyright (c) 2017 Ruslan Babayev <ruslan@babayev.com>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <sys/types.h>
#include <sys/queue.h>
#include <sys/tree.h>
#include <sys/socket.h>
#include <net/if.h>
#include <linux/if.h>
#include <linux/netlink.h>
#include <linux/rtnetlink.h>

#include <string.h>
#include <stdlib.h>

#include <confd_lib.h>
#include <confd_cdb.h>
#include <confd_dp.h>

#include "linkd.h"
#include "netlink.h"
#include "log.h"
#include "ietf-routing.h"
#include "ietf-ipv4-unicast-routing.h"
#include "ietf-ipv6-unicast-routing.h"

#define RIB_IPV4	"ipv4"
#define RIB_IPV6	"ipv6"

/* forward declarations */
struct route;
union ip;

int	rt4_cmp(struct route *, struct route *);
int	rt4_init(void);
int	rt6_cmp(struct route *, struct route *);
int	rt6_init(void);
struct route *
	rt_find(int family, union ip *, int);
struct route *
	rt_insert(int, union ip *, int);
void	rt_remove(struct route *);
void	rt_add_multipath(struct route *, int, union ip *);
void	handle_multipath(struct route *, struct rtnexthop *, int);
int	rt_get_next_rib(struct confd_trans_ctx *, confd_hkeypath_t *, long);
int	rt_get_next_ipv4_route(struct confd_trans_ctx *, confd_hkeypath_t *,
    long);
int	rt_get_next_ipv6_route(struct confd_trans_ctx *, confd_hkeypath_t *,
    long);
int	rt_get_next_nexthop(struct confd_trans_ctx *, confd_hkeypath_t *, long);
int	rt_get_elem_rt(struct confd_trans_ctx *, confd_hkeypath_t *);
int	rt_get_elem_rt_af(struct confd_trans_ctx *, confd_hkeypath_t *);
int	rt_get_elem_rt_source_proto(struct confd_trans_ctx *,
    confd_hkeypath_t *);
int	rt_get_elem_rt_oif(struct confd_trans_ctx *, confd_hkeypath_t *);
int	rt_get_elem_rt_spec_nh(struct confd_trans_ctx *, confd_hkeypath_t *);
int	rt_get_elem_v4ur(struct confd_trans_ctx *, confd_hkeypath_t *);
int	rt_get_elem_v6ur(struct confd_trans_ctx *, confd_hkeypath_t *);
struct route *
	rt_get_route(confd_hkeypath_t *);
enum cdb_iter_ret
	rt_iter_simple(confd_hkeypath_t *, int, void *, int);
enum cdb_iter_ret
	rt_iter_special(confd_hkeypath_t *, int, void *, int);
enum cdb_iter_ret
	rt_iter_list(confd_hkeypath_t *, int, void *, int);

union ip {
	struct in_addr		 v4;
	struct in6_addr		 v6;
} any = { 0 };

struct prefix {
	union ip		 ip;
	uint8_t			 len;
};

struct nexthop {
	TAILQ_ENTRY(nexthop)	 entry;
	union ip		 ip;
	int			 ifindex;
};

struct route {
	RB_ENTRY(route)		 entry;
	struct prefix		 prefix;
	struct nexthop		 nexthop;
	uint8_t			 family;
	uint8_t			 table;
	uint8_t			 protocol;
	uint8_t			 type;
	uint32_t		 flags;
	uint32_t		 priority;
	TAILQ_HEAD(, nexthop)	 multipath;
};

RB_HEAD(rt4, route) rt4 = RB_INITIALIZER();
RB_PROTOTYPE(rt4, route, entry, rt4_cmp)
RB_GENERATE(rt4, route, entry, rt4_cmp)

RB_HEAD(rt6, route) rt6 = RB_INITIALIZER();
RB_PROTOTYPE(rt6, route, entry, rt6_cmp)
RB_GENERATE(rt6, route, entry, rt6_cmp)

int
rt4_cmp(struct route *a, struct route *b)
{
	assert(a->family == b->family);
	assert(a->family == AF_INET);

	if (ntohl(a->prefix.ip.v4.s_addr) < ntohl(b->prefix.ip.v4.s_addr))
		return (-1);
	if (ntohl(a->prefix.ip.v4.s_addr) > ntohl(b->prefix.ip.v4.s_addr))
		return (1);

	if (a->prefix.len < b->prefix.len)
		return (-1);
	if (a->prefix.len > b->prefix.len)
		return (1);

	if (a->protocol == RTPROT_UNSPEC || b->protocol == RTPROT_UNSPEC)
		return (0);
	if (a->protocol < b->protocol)
		return (-1);
	if (a->protocol > b->protocol)
		return (1);

	return (0);
}

int
rt4_init(void)
{
	RB_INIT(&rt4);
	return getroute(RT_TABLE_MAIN, AF_INET);
}

int
rt6_cmp(struct route *a, struct route *b)
{
	int rv;

	assert(a->family == b->family);
	assert(a->family == AF_INET6);

	rv = memcmp(&a->prefix.ip.v6, &b->prefix.ip.v6,
		    sizeof(struct in6_addr));
	if (rv)
		return (rv);

	if (a->prefix.len < b->prefix.len)
		return (-1);
	if (a->prefix.len > b->prefix.len)
		return (1);

	if (a->protocol == RTPROT_UNSPEC || b->protocol == RTPROT_UNSPEC)
		return (0);
	if (a->protocol < b->protocol)
		return (-1);
	if (a->protocol > b->protocol)
		return (1);

	return (0);
}

int
rt6_init(void)
{
	RB_INIT(&rt6);
	return getroute(RT_TABLE_MAIN, AF_INET6);
}

static inline int
addrlen(int family)
{
	return family == AF_INET ?
	    sizeof(struct in_addr) :
	    sizeof(struct in6_addr);
}

struct route *
rt_find(int family, union ip *ip, int prefixlen)
{
	struct route	s;

	memset(&s, 0, sizeof(s));
	s.family = family;
	memcpy(&s.prefix.ip, ip, addrlen(family));
	s.prefix.len = prefixlen;

	if (family == AF_INET)
		return RB_FIND(rt4, &rt4, &s);
	else
		return RB_FIND(rt6, &rt6, &s);
}

struct route *
rt_insert(int family, union ip *ip, int prefixlen)
{
	struct route	*new, *old;

	assert(family == AF_INET || family == AF_INET6);

	if ((new = calloc(1, sizeof(struct route))) == NULL)
		return (NULL);

	new->family = family;
	memcpy(&new->prefix.ip, ip, addrlen(family));
	new->prefix.len = prefixlen;
	TAILQ_INIT(&new->multipath);

	if (family == AF_INET)
		old = RB_INSERT(rt4, &rt4, new);
	else
		old = RB_INSERT(rt6, &rt6, new);

	if (old) {
		free(new);
		return old;
	} else {
		return new;
	}
}

void
rt_remove(struct route *r)
{
	struct nexthop	*nh, *tnh;

	assert(r->family == AF_INET || r->family == AF_INET6);

	if (r->family == AF_INET)
		RB_REMOVE(rt4, &rt4, r);
	else
		RB_REMOVE(rt6, &rt6, r);

	TAILQ_FOREACH_SAFE(nh, &r->multipath, entry, tnh) {
		TAILQ_REMOVE(&r->multipath, nh, entry);
		free(nh);
	}

	free(r);
}

void
rt_add_multipath(struct route *r, int ifindex, union ip *ip)
{
	struct nexthop	*nh;

	TAILQ_FOREACH(nh, &r->multipath, entry) {
		if (nh->ifindex == ifindex &&
		    !memcmp(&nh->ip, ip, addrlen(r->family)))
				break;
	}
	if (nh)
		return;

	nh = calloc(1, sizeof(*nh));
	if (!nh)
		fatal("rt_add_multipath: calloc");

	nh->ifindex = ifindex;
	memcpy(&nh->ip, ip, addrlen(r->family));
	TAILQ_INSERT_TAIL(&r->multipath, nh, entry);
}

int
rt_init(void)
{
	int	rv;

	rv = rt4_init();
	if (rv < 0)
		return rv;

	return rt6_init();
}

/* ARGSUSED */
enum cdb_iter_ret
rt_iter(confd_hkeypath_t *kp, enum cdb_iter_op op, confd_value_t *oldv,
    confd_value_t *newv, void *state)
{
	struct		 confd_identityref proto;
	struct		 confd_ipv4_prefix p4;
	struct		 confd_ipv6_prefix p6;
	void		*prefix;
	confd_value_t	 nho;
	int		 rv, afi, plen, tag, i = kp->len;
	char		 path[256];

	--i; tag = CONFD_GET_XMLTAG(&kp->v[i][0]);
	if (tag != rt_routing)
		return ITER_CONTINUE;

	--i; tag = CONFD_GET_XMLTAG(&kp->v[i][0]);
	if (tag != rt_control_plane_protocols)
		return ITER_CONTINUE;

	--i; tag = CONFD_GET_XMLTAG(&kp->v[i][0]);
	if (tag != rt_control_plane_protocol)
		return ITER_CONTINUE;

	--i; proto = CONFD_GET_IDENTITYREF(&kp->v[i][0]);
	if (proto.ns != rt__ns || proto.id != rt_static)
		return ITER_CONTINUE;

	if (i < 1)
		return ITER_RECURSE;

	--i; tag = CONFD_GET_XMLTAG(&kp->v[i][0]);
	if (tag != rt_static_routes)
		return ITER_CONTINUE;

	--i; tag = CONFD_GET_XMLTAG(&kp->v[i][0]);
	switch (tag) {
	case rt_ipv4:
		afi = AF_INET;
		break;
	case rt_ipv6:
		afi = AF_INET6;
		break;
	default:
		return ITER_CONTINUE;
	}

	--i; tag = CONFD_GET_XMLTAG(&kp->v[i][0]);
	if (tag != rt_route)
		return ITER_CONTINUE;

	switch (afi) {
	case AF_INET:
		--i; p4 = CONFD_GET_IPV4PREFIX(&kp->v[i][0]);
		prefix = &p4.ip;
		plen = p4.len;
		break;
	case AF_INET6:
		--i; p6 = CONFD_GET_IPV6PREFIX(&kp->v[i][0]);
		prefix = &p6.ip6;
		plen = p6.len;
		break;
	default:
		return ITER_CONTINUE;
	}

	if (i < 1) {
		if (op != MOP_DELETED)
			return ITER_RECURSE;
		if (delroute(RT_TABLE_MAIN, afi, prefix, plen) < 0)
			log_warn("rt_iter_delroute: delroute");
		return ITER_CONTINUE;
	}

	--i; tag = CONFD_GET_XMLTAG(&kp->v[i][0]);
	if (tag != rt_next_hop)
		return ITER_CONTINUE;

	/* produce path up to next-hop */
	confd_pp_kpath_len(path, sizeof path, kp, 9);
	cdb_cd(ds, "%s", path);

	rv = cdb_get_case(ds, "next-hop-options", &nho, path);
	if (rv != CONFD_OK)
		fatal("rt_iter: cdb_get_case");

	tag = CONFD_GET_XMLTAG(&nho);
	switch (tag) {
	case rt_simple_next_hop:
		return rt_iter_simple(kp, afi, prefix, plen);
	case rt_special_next_hop:
		return rt_iter_special(kp, afi, prefix, plen);
	case rt_next_hop_list:
		return rt_iter_list(kp, afi, prefix, plen);
	}

	return ITER_CONTINUE;
}

enum cdb_iter_ret
rt_iter_simple(confd_hkeypath_t *kp, int afi, void *prefix, int plen)
{
	union ip	 nh;
	union ip	*pnh = NULL;
	int		 ifindex = 0;
	const int	 table = RT_TABLE_MAIN;
	const int	 type = RTN_UNICAST;
	char		 ifname[IF_NAMESIZE];

	if (cdb_exists(ds, "next-hop-address")) {
		switch (afi) {
		case AF_INET:
			if (cdb_get_ipv4(ds, &nh.v4, "next-hop-address") < 0)
				fatal("rt_iter_simple: cdb_get_ipv4");
			break;
		case AF_INET6:
			if (cdb_get_ipv6(ds, &nh.v6, "next-hop-address") < 0)
				fatal("rt_iter_simple: cdb_get_ipv6");
			break;
		default:
			return ITER_CONTINUE;
		}
		pnh = &nh;
	}

	if (cdb_exists(ds, "outgoing-interface")) {
		if (cdb_get_str(ds, ifname, sizeof ifname,
			"outgoing-interface") < 0)
			fatal("rt_iter_simple: cdb_get_str");
		ifindex = if_nametoindex(ifname);
	}

	if (newroute(table, afi, type, prefix, plen, pnh, ifindex) < 0)
		log_warn("rt_iter_simple: newroute");

	return ITER_UP;
}

enum cdb_iter_ret
rt_iter_special(confd_hkeypath_t *kp, int afi, void *prefix, int plen)
{
	const int	table = RT_TABLE_MAIN;
	int		snh, type;

	if (cdb_get_enum_value(ds, &snh, "special-next-hop") < 0)
		fatal("rt_iter_special: cdb_get_enum_value: special-next-hop");

	switch (snh) {
	case rt_blackhole:
		type = RTN_BLACKHOLE;
		break;
	case rt_unreachable:
		type = RTN_UNREACHABLE;
		break;
	case rt_prohibit:
		type = RTN_PROHIBIT;
		break;
	default:
		log_warnx("rt_iter_special: unhandled special-next-hop %d",
		    snh);
		return ITER_UP;
	}

	if (newroute(table, afi, type, prefix, plen, NULL, 0) < 0)
		log_warn("rt_iter_special: newroute");

	return ITER_UP;
}

enum cdb_iter_ret
rt_iter_list(confd_hkeypath_t *kp, int afi, void *prefix, int plen)
{
	log_info("rt_iter_list: not supported yet");
	return ITER_UP;
}

int
rt_get_elem(struct confd_trans_ctx *tctx, confd_hkeypath_t *kp)
{
	int	ns, tag, i = kp->len;

	--i; tag = CONFD_GET_XMLTAG(&kp->v[i][0]);
	if (tag != rt_routing)
		goto not_found;

	--i; tag = CONFD_GET_XMLTAG(&kp->v[i][0]);
	switch (tag) {
	case rt_control_plane_protocols:
		break;
	case rt_ribs:
		ns = CONFD_GET_XML_NS(&kp->v[0][0]);
		switch (ns) {
		case rt__ns:
			return rt_get_elem_rt(tctx, kp);
		case v4ur__ns:
			return rt_get_elem_v4ur(tctx, kp);
		case v6ur__ns:
			return rt_get_elem_v6ur(tctx, kp);
		default:
			log_warnx("rt_get_elem: unsupported namespace %d", ns);
			return CONFD_ERR;
		}
		break;
	}

not_found:
	confd_data_reply_not_found(tctx);
	return CONFD_OK;
}

struct route *
rt_get_route(confd_hkeypath_t *kp)
{
	assert(kp->len >= 7);
	return (struct route *)CONFD_GET_INT64(&kp->v[kp->len - 7][0]);
}

int
rt_get_elem_rt(struct confd_trans_ctx *tctx, confd_hkeypath_t *kp)
{
	struct route	*r;
	confd_value_t	 v;
	int		 tag;

	tag = CONFD_GET_XML(&kp->v[0][0]);
	switch (tag) {
	case rt_router_id:
		goto not_found;
	case rt_interface:
		goto not_found;
	case rt_name:
		confd_value_dup_to(&kp->v[1][0], &v);
		break;
	case rt_default_rib:
		goto not_found;
	case rt_address_family:
		return rt_get_elem_rt_af(tctx, kp);
	case rt_route_preference:
		r = rt_get_route(kp);
		if (!r->priority)
			goto not_found;
		CONFD_SET_UINT32(&v, r->priority);
		break;
	case rt_last_updated:
		goto not_found;
	case rt_source_protocol:
		return rt_get_elem_rt_source_proto(tctx, kp);
	case rt_outgoing_interface:
		return rt_get_elem_rt_oif(tctx, kp);
	case rt_special_next_hop:
		return rt_get_elem_rt_spec_nh(tctx, kp);
	default:
		log_warnx("rt_get_elem_rt: unhandled tag %s",
		    confd_hash2str(tag));
		goto not_found;
	}

	confd_data_reply_value(tctx, &v);
	return CONFD_OK;

not_found:
	confd_data_reply_not_found(tctx);
	return CONFD_OK;
}

int
rt_get_elem_rt_af(struct confd_trans_ctx *tctx, confd_hkeypath_t *kp)
{
	struct		 confd_identityref idref;
	confd_value_t	 v;
	char		*rib;

	rib = CONFD_GET_CBUFPTR(&kp->v[1][0]);
	if (!strcmp(rib, RIB_IPV4)) {
		idref.id = v4ur_ipv4_unicast;
		idref.ns = v4ur__ns;
		CONFD_SET_IDENTITYREF(&v, idref);
	} else if (!strcmp(rib, RIB_IPV6)) {
		idref.id = v6ur_ipv6_unicast;
		idref.ns = v6ur__ns;
		CONFD_SET_IDENTITYREF(&v, idref);
	} else {
		confd_data_reply_not_found(tctx);
		return CONFD_OK;
	}

	confd_data_reply_value(tctx, &v);
	return CONFD_OK;
}

int
rt_get_elem_rt_source_proto(struct confd_trans_ctx *tctx, confd_hkeypath_t *kp)
{
	struct		 confd_identityref idref;
	struct route	*r;
	confd_value_t	 v;

	r = rt_get_route(kp);
	switch (r->protocol) {
	case RTPROT_KERNEL:
	case RTPROT_BOOT:
		idref.ns = rt__ns;
		idref.id = rt_direct;
		CONFD_SET_IDENTITYREF(&v, idref);
		break;
	case RTPROT_STATIC:
		idref.ns = rt__ns;
		idref.id = rt_static;
		CONFD_SET_IDENTITYREF(&v, idref);
		break;
	default:
		confd_data_reply_not_found(tctx);
		return CONFD_OK;
	}

	confd_data_reply_value(tctx, &v);
	return CONFD_OK;
}

int
rt_get_elem_rt_oif(struct confd_trans_ctx *tctx, confd_hkeypath_t *kp)
{
	struct route	*r;
	struct nexthop	*nh;
	confd_value_t	 v;
	char		 ifname[IF_NAMESIZE];

	if (kp->len == 12) {
		nh = (struct nexthop *)CONFD_GET_INT64(&kp->v[1][0]);
	} else {
		r = rt_get_route(kp);
		nh = &r->nexthop;
	}

	if (!if_indextoname(nh->ifindex, ifname)) {
		log_warn("rt_get_elem_rt_oif: if_indextoname: ifindex %d",
		    nh->ifindex);
		return CONFD_ERR;
	}

	CONFD_SET_STR(&v, ifname);
	confd_data_reply_value(tctx, &v);
	return CONFD_OK;
}

int
rt_get_elem_rt_spec_nh(struct confd_trans_ctx *tctx, confd_hkeypath_t *kp)
{
	struct route	*r;
	confd_value_t	 v;

	r = rt_get_route(kp);
	switch (r->type) {
	case RTN_BLACKHOLE:
		CONFD_SET_ENUM_VALUE(&v, rt_blackhole);
		break;
	case RTN_UNREACHABLE:
		CONFD_SET_ENUM_VALUE(&v, rt_unreachable);
		break;
	case RTN_PROHIBIT:
		CONFD_SET_ENUM_VALUE(&v, rt_prohibit);
		break;
	default:
		confd_data_reply_not_found(tctx);
		return CONFD_OK;
	}

	confd_data_reply_value(tctx, &v);
	return CONFD_OK;
}

int
rt_get_elem_v4ur(struct confd_trans_ctx *tctx, confd_hkeypath_t *kp)
{
	struct		 confd_ipv4_prefix prefix;
	struct route	*r;
	struct nexthop	*nh;
	confd_value_t	 v;
	int		 tag;

	r = rt_get_route(kp);

	tag = CONFD_GET_XML(&kp->v[0][0]);
	switch (tag) {
	case v4ur_destination_prefix:
		prefix.ip = r->prefix.ip.v4;
		prefix.len = r->prefix.len;
		CONFD_SET_IPV4PREFIX(&v, prefix);
		break;
	case v4ur_next_hop_address:
		if (!TAILQ_EMPTY(&r->multipath))
			goto not_found;
		if (r->nexthop.ip.v4.s_addr != INADDR_ANY)
			CONFD_SET_IPV4(&v, r->nexthop.ip.v4);
		else
			goto not_found;
		break;
	case v4ur_address:
		if (TAILQ_EMPTY(&r->multipath))
			goto not_found;
		nh = (struct nexthop *)CONFD_GET_INT64(&kp->v[1][0]);
		if (nh->ip.v4.s_addr != INADDR_ANY)
			CONFD_SET_IPV4(&v, nh->ip.v4);
		else
			goto not_found;
		break;
	default:
		log_warnx("rt_get_elem_v4ur: unhandled tag %s",
		    confd_hash2str(tag));
		return CONFD_ERR;
	}
	confd_data_reply_value(tctx, &v);
	return CONFD_OK;

not_found:
	confd_data_reply_not_found(tctx);
	return CONFD_OK;
}

int
rt_get_elem_v6ur(struct confd_trans_ctx *tctx, confd_hkeypath_t *kp)
{
	struct		 confd_ipv6_prefix prefix;
	struct route	*r;
	struct nexthop	*nh;
	confd_value_t	 v;
	int		 tag;

	r = rt_get_route(kp);

	tag = CONFD_GET_XML(&kp->v[0][0]);
	switch (tag) {
	case v6ur_destination_prefix:
		prefix.ip6 = r->prefix.ip.v6;
		prefix.len = r->prefix.len;
		CONFD_SET_IPV6PREFIX(&v, prefix);
		break;
	case v6ur_next_hop_address:
		if (!TAILQ_EMPTY(&r->multipath))
			goto not_found;
		if (memcmp(&r->nexthop.ip, &in6addr_any, sizeof in6addr_any))
			CONFD_SET_IPV6(&v, r->nexthop.ip.v6);
		else
			goto not_found;
		break;
	case v6ur_address:
		if (TAILQ_EMPTY(&r->multipath))
			goto not_found;
		nh = (struct nexthop *)CONFD_GET_INT64(&kp->v[1][0]);
		if (memcmp(&nh->ip, &in6addr_any, sizeof in6addr_any))
			CONFD_SET_IPV6(&v, nh->ip.v6);
		else
			goto not_found;
		break;
	default:
		log_warnx("rt_get_elem_v6ur: unhandled tag %s",
		    confd_hash2str(tag));
		return CONFD_ERR;
	}
	confd_data_reply_value(tctx, &v);
	return CONFD_OK;

not_found:
	confd_data_reply_not_found(tctx);
	return CONFD_OK;
}

int
rt_get_next(struct confd_trans_ctx *tctx, confd_hkeypath_t *kp, long next)
{
	int	 tag, i = kp->len;
	char	*rib;

	--i; tag = CONFD_GET_XMLTAG(&kp->v[i][0]);
	if (tag != rt_routing)
		goto not_found;

	--i; tag = CONFD_GET_XMLTAG(&kp->v[i][0]);
	switch (tag) {
	case rt_interfaces:
		break;
	case rt_control_plane_protocols:
		break;
	case rt_ribs:
		tag = CONFD_GET_XMLTAG(&kp->v[0][0]);
		switch (tag) {
		case rt_rib:
			return rt_get_next_rib(tctx, kp, next);
		case rt_route:
			rib = CONFD_GET_CBUFPTR(&kp->v[kp->len - 4][0]);
			if (!strcmp(rib, RIB_IPV4))
				return rt_get_next_ipv4_route(tctx, kp, next);
			else if (!strcmp(rib, RIB_IPV6))
				return rt_get_next_ipv6_route(tctx, kp, next);
			else
				log_warnx("rt_get_next: unknown rib %s", rib);
			break;
		case rt_next_hop:
			return rt_get_next_nexthop(tctx, kp, next);
		}
		break;
	}

not_found:
	confd_data_reply_next_key(tctx, NULL, 0, 0);
	return CONFD_OK;
}

int
rt_get_next_rib(struct confd_trans_ctx *tctx, confd_hkeypath_t *kp, long next)
{
	confd_value_t	 v;

	switch (next) {
	case -1:
		CONFD_SET_STR(&v, RIB_IPV4);
		break;
	case 0:
		CONFD_SET_STR(&v, RIB_IPV6);
		break;
	default:
		confd_data_reply_next_key(tctx, NULL, 0, 0);
		return CONFD_OK;
	}

	confd_data_reply_next_key(tctx, &v, 1, next + 1);
	return CONFD_OK;
}

int
rt_get_next_ipv4_route(struct confd_trans_ctx *tctx, confd_hkeypath_t *kp,
    long next)
{
	struct route	*r;
	confd_value_t	 v;

	r = next == -1 ? RB_MIN(rt4, &rt4) : (struct route *)next;
	if (!r) {
		confd_data_reply_next_key(tctx, NULL, 0, 0);
	} else {
		CONFD_SET_INT64(&v, (long)r);
		next = (long)RB_NEXT(rt4, &rt4, r);
		confd_data_reply_next_key(tctx, &v, 1, next);
	}

	return CONFD_OK;
}

int
rt_get_next_ipv6_route(struct confd_trans_ctx *tctx, confd_hkeypath_t *kp,
    long next)
{
	struct route	*r;
	confd_value_t	 v;

	r = next == -1 ? RB_MIN(rt6, &rt6) : (struct route *)next;
	if (!r) {
		confd_data_reply_next_key(tctx, NULL, 0, 0);
	} else {
		CONFD_SET_INT64(&v, (long)r);
		next = (long)RB_NEXT(rt6, &rt6, r);
		confd_data_reply_next_key(tctx, &v, 1, next);
	}

	return CONFD_OK;
}

int
rt_get_next_nexthop(struct confd_trans_ctx *tctx, confd_hkeypath_t *kp,
    long next)
{
	struct route	*r;
	struct nexthop	*nh;
	confd_value_t	 v;

	r = rt_get_route(kp);
	if (TAILQ_EMPTY(&r->multipath)) {
		confd_data_reply_next_key(tctx, NULL, 0, 0);
		return CONFD_OK;
	}

	nh = next == -1 ? TAILQ_FIRST(&r->multipath) : (struct nexthop *)next;
	if (!nh) {
		confd_data_reply_next_key(tctx, NULL, 0, 0);
	} else {
		CONFD_SET_INT64(&v, (long)nh);
		next = (long)TAILQ_NEXT(nh, entry);
		confd_data_reply_next_key(tctx, &v, 1, next);
	}

	return CONFD_OK;
}

/* ARGSUSED */
int
rt_get_case(struct confd_trans_ctx *tctx, confd_hkeypath_t *kp,
    confd_value_t *choice)
{
	struct route	*r;
	confd_value_t	 v;
	int		 tag;

	r = rt_get_route(kp);
	switch (r->type) {
	case RTN_UNICAST:
		if (TAILQ_EMPTY(&r->multipath))
			tag = rt_simple_next_hop;
		else
			tag = rt_next_hop_list;
		break;
	case RTN_BLACKHOLE:
	case RTN_UNREACHABLE:
	case RTN_PROHIBIT:
		tag = rt_special_next_hop;
		break;
	default:
		return CONFD_ERR;
	}

	CONFD_SET_XMLTAG(&v, tag, rt__ns);
	confd_data_reply_value(tctx, &v);
	return CONFD_OK;
}

int
rt_exists_optional(struct confd_trans_ctx *tctx, confd_hkeypath_t *kp)
{
	int	 tag;

	tag = CONFD_GET_XMLTAG(&kp->v[0][0]);
	if (tag != rt_active)
		confd_data_reply_not_found(tctx);
	else
		confd_data_reply_found(tctx);

	return CONFD_OK;
}

int
handle_newroute(struct nlmsghdr *nlmsg)
{
	struct rtattr	*tb[RTA_MAX + 1];
	struct rtattr	*rta;
	struct rtmsg	*rtm;
	struct route	*r;
	union ip	*ip;
	int		 len;
	char		 str[256];

	len = RTM_PAYLOAD(nlmsg);
	if (len < 0)
		return (-1);

	rtm = NLMSG_DATA(nlmsg);
	if (rtm->rtm_table != RT_TABLE_MAIN)
		return (0);

	rtattr_parse(RTM_RTA(rtm), len, tb, RTA_MAX);

	rta = tb[RTA_DST];
	ip = rta ? RTA_DATA(rta) : &any;
	inet_ntop(rtm->rtm_family, ip, str, sizeof str);
	log_debug("handle_newroute: family:%d type:%d flags:0x%x %s/%d",
	    rtm->rtm_family, rtm->rtm_type, rtm->rtm_flags,
	    str, rtm->rtm_dst_len);

	r = rt_insert(rtm->rtm_family, ip, rtm->rtm_dst_len);
	if (!r) {
		log_warnx("handle_newroute: rt_insert");
		return (0);
	}

	r->family	= rtm->rtm_family;
	r->table	= rtm->rtm_table;
	r->protocol	= rtm->rtm_protocol;
	r->type		= rtm->rtm_type;
	r->flags	= rtm->rtm_flags;

	rta = tb[RTA_GATEWAY];
	if (rta)
		memcpy(&r->nexthop.ip, RTA_DATA(rta), RTA_PAYLOAD(rta));
	else
		memset(&r->nexthop.ip, 0, sizeof (r->nexthop.ip));

	rta = tb[RTA_OIF];
	if (rta)
		r->nexthop.ifindex = rtattr_get_u32(rta);

	rta = tb[RTA_PRIORITY];
	if (rta)
		r->priority = rtattr_get_u32(rta);

	rta = tb[RTA_MULTIPATH];
	if (rta)
		handle_multipath(r, RTA_DATA(rta), RTA_PAYLOAD(rta));

	return (0);
}

void
handle_multipath(struct route *r, struct rtnexthop *rtnh, int len)
{
	struct rtattr	*tb[RTA_MAX + 1];
	struct rtattr	*rta;
	union ip	*ip;
	int		 alen;

	for (; RTNH_OK(rtnh, len); rtnh = RTNH_NEXT(rtnh)) {
		alen = rtnh->rtnh_len - sizeof(*rtnh);
		if (alen > 0) {
			rtattr_parse(RTNH_DATA(rtnh), alen, tb, RTA_MAX);
			rta = tb[RTA_GATEWAY];
			ip = rta ? RTA_DATA(rta) : &any;
		} else {
			ip = &any;
		}
		rt_add_multipath(r, rtnh->rtnh_ifindex, ip);
	}
}

int
handle_delroute(struct nlmsghdr *nlmsg)
{
	struct rtattr	*tb[RTA_MAX + 1];
	struct rtattr	*rta;
	struct rtmsg	*rtm;
	struct route	*r;
	union ip	*ip;
	int		 len;
	char		 str[256];

	len = RTM_PAYLOAD(nlmsg);
	if (len < 0)
		return (-1);

	rtm = NLMSG_DATA(nlmsg);
	if (rtm->rtm_table != RT_TABLE_MAIN)
		return (0);

	rtattr_parse(RTM_RTA(rtm), len, tb, RTA_MAX);

	rta = tb[RTA_DST];
	ip = rta ? RTA_DATA(rta) : &any;
	inet_ntop(rtm->rtm_family, ip, str, sizeof str);
	log_debug("handle_delroute: family:%d, type:%d, flags:0x%x, %s/%d",
	    rtm->rtm_family, rtm->rtm_type, rtm->rtm_flags,
	    str, rtm->rtm_dst_len);

	r = rt_find(rtm->rtm_family, ip, rtm->rtm_dst_len);
	if (!r) {
		log_warnx("handle_delroute: route %s/%d not found",
		    str, rtm->rtm_dst_len);
		return (0);
	}

	rt_remove(r);
	return (0);
}
