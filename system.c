/*
 * Copyright (c) 2017 Ruslan Babayev <ruslan@babayev.com>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <sys/types.h>
#include <sys/sysinfo.h>
#include <sys/utsname.h>
#include <sys/time.h>
#include <sys/stat.h>

#include <errno.h>
#include <pwd.h>
#include <shadow.h>
#include <string.h>
#include <signal.h>
#include <time.h>
#include <unistd.h>
#include <stdlib.h>

#include <confd_lib.h>
#include <confd_cdb.h>
#include <confd_dp.h>

#include "linkd.h"
#include "log.h"
#include "base64.h"
#include "ietf-system.h"

#define PATH_HOSTNAME	"/etc/hostname"
#define PATH_TIMEZONE	"/etc/timezone"
#define PATH_TIMEZONE_TMP PATH_TIMEZONE ".tmp"
#define PATH_LOCALTIME	"/etc/localtime"
#define PATH_ZONEINFO	"/usr/share/zoneinfo"
#define PATH_ZONETAB	PATH_ZONEINFO "/zone.tab"
#define MAXTZ		512
#define MAXNS		3
#define PATH_RESCONF	"/etc/resolv.conf"
#define PATH_PASSWD	"/etc/passwd"
#define PATH_SHADOW	"/etc/shadow"
#define PATH_CONFD_CLI	"/usr/bin/confd_cli"
#define AAA_AUTH_USERS	"/aaa/authentication/users"

enum cdb_iter_ret
	sys_iter_clock(confd_hkeypath_t *, enum cdb_iter_op,
	    confd_value_t *);
enum cdb_iter_ret
	sys_iter_hostname(confd_hkeypath_t *, enum cdb_iter_op,
	    confd_value_t *);
enum cdb_iter_ret
	sys_iter_ntp(confd_hkeypath_t *, enum cdb_iter_op,
	    confd_value_t *);
enum cdb_iter_ret
	sys_iter_resolver(confd_hkeypath_t *, enum cdb_iter_op,
	    confd_value_t *);
enum cdb_iter_ret
	sys_iter_radius(confd_hkeypath_t *, enum cdb_iter_op,
	    confd_value_t *);
enum cdb_iter_ret
	sys_iter_auth(confd_hkeypath_t *, enum cdb_iter_op,
	    confd_value_t *);

int	make_dir(char *, mode_t, uid_t, gid_t);
int	update_passwd(struct passwd *, enum cdb_iter_op);
int	update_shadow(struct spwd *, enum cdb_iter_op);
int	write_authorized_keys(char *, uid_t, gid_t);

/* ARGSUSED */
int sys_restart(struct confd_user_info *ui, struct xml_tag *name,
    confd_hkeypath_t *kp, confd_tag_value_t *params, int nparams)
{
	return kill(1, SIGTERM);
}

/* ARGSUSED */
int sys_shutdown(struct confd_user_info *ui, struct xml_tag *name,
    confd_hkeypath_t *kp, confd_tag_value_t *params, int nparams)
{
	return kill(1, SIGUSR2);
}

/* ARGSUSED */
int
sys_setdatetime(struct confd_user_info *uinfo, struct xml_tag *name,
    confd_hkeypath_t *kp, confd_tag_value_t *params, int nparams)
{
	struct confd_datetime	 dt;
	struct tm		 tm;
	struct timeval		 tv;
	confd_value_t		*v;

	v = CONFD_GET_TAG_VALUE(&params[0]);
	dt = CONFD_GET_DATETIME(v);

	memset(&tm, 0, sizeof tm);
	tm.tm_year	= dt.year - 1900;
	tm.tm_mon	= dt.month - 1;
	tm.tm_mday	= dt.day;
	tm.tm_hour	= dt.hour;
	tm.tm_min	= dt.min;
	tm.tm_sec	= dt.sec;
	tm.tm_isdst	= -1;

	tv.tv_sec	= mktime(&tm);
	tv.tv_usec	= 0;

	if (settimeofday(&tv, NULL) < 0)
		log_warn("settimeofday");

	return CONFD_OK;
}

int
sys_tz_validate(struct confd_trans_ctx *tctx, confd_hkeypath_t *kp,
    confd_value_t *newv)
{
	int	 tag;
	char	*tz;
	char	 path[256];

	tag = CONFD_GET_XMLTAG(&kp->v[0][0]);
	if (tag != sys_timezone_name || !newv)
		return CONFD_OK;

	tz = CONFD_GET_CBUFPTR(newv);
	snprintf(path, sizeof path, "%s/%s", PATH_ZONEINFO, tz);
	if (access(path, F_OK) < 0) {
		confd_trans_seterr(tctx, "timezone \"%s\" not found", tz);
		return CONFD_ERR;
	}

	return CONFD_OK;
}

/* ARGSUSED */
int
sys_tz_complete(struct confd_user_info *uinfo, int cli_style, char *token,
    int completion_char, confd_hkeypath_t *kp, char *cmdpath,
    char *cmdparam_id, struct confd_qname *simple_type, char *extra)
{
	struct	 confd_completion_value *cv;
	FILE	*zonetab;
	char	 line[256];
	char	*cp;
	int	 i, n;

	zonetab = fopen(PATH_ZONETAB, "r");
	if (!zonetab)
		fatal("sys_tz_complete: failed opening " PATH_ZONETAB);

	cv = calloc(MAXTZ, sizeof *cv);
	if (!cv)
		fatal("sys_tz_complete: calloc");

	i = 0;
	while (!feof(zonetab) && i < MAXTZ) {
		cp = fgets(line, sizeof line, zonetab);
		if (cp != line)
			break;

		/* skip comments */
		if (line[0] == '#')
			continue;

		(void)strtok(line, "\t"); /* country */
		(void)strtok(NULL, "\t"); /* coordinates */
		cp = strtok(NULL, "\t\n"); /* timezone */
		cv[i].value = strdup(cp);
		cp = strtok(NULL, "\n"); /* comments */
		cv[i].extra = cp ? strdup(cp) : NULL;
		cv[i].type = CONFD_COMPLETION;
		i++;
	}
	fclose(zonetab);
	n = i;

	if (n == MAXTZ)
		log_warn("sys_tz_complete: found more than %d timezones",
		    MAXTZ);

	if (confd_action_reply_completion(uinfo, cv, n) < 0)
		fatal("sys_tz_complete: confd_action_reply_completion");

	for (i = 0; i < n; i++) {
		free(cv[i].value);
		free(cv[i].extra);
	}
	free(cv);

	return CONFD_OK;
}

/* ARGSUSED */
enum cdb_iter_ret
sys_iter(confd_hkeypath_t *kp, enum cdb_iter_op op, confd_value_t *oldv,
    confd_value_t *newv, void *state)
{
	int	 tag, i = kp->len;

	--i; tag = CONFD_GET_XMLTAG(&kp->v[i][0]);
	if (tag != sys_system)
		return ITER_CONTINUE;

	--i; tag = CONFD_GET_XMLTAG(&kp->v[i][0]);
	switch (tag) {
	case sys_clock:
		return sys_iter_clock(kp, op, newv);
	case sys_hostname:
		return sys_iter_hostname(kp, op, newv);
	case sys_ntp:
		return sys_iter_ntp(kp, op, newv);
	case sys_dns_resolver:
		return sys_iter_resolver(kp, op, newv);
	case sys_radius:
		return sys_iter_radius(kp, op, newv);
	case sys_authentication:
		return sys_iter_auth(kp, op, newv);
	}

	return ITER_CONTINUE;
}

enum cdb_iter_ret
sys_iter_clock(confd_hkeypath_t *kp, enum cdb_iter_op op,
    confd_value_t *newv)
{
	FILE	*file;
	char	*tz;
	int	 tag;
	char	 path[256];

	if (op == MOP_DELETED) {
		(void)unlink(PATH_TIMEZONE);
		(void)unlink(PATH_LOCALTIME);
		return ITER_CONTINUE;
	}

	tag = CONFD_GET_XMLTAG(&kp->v[kp->len - 3][0]);
	if (tag == sys_timezone_utc_offset) {
		log_debug("sys_iter_timezone: timezone-utc-offset %d",
		    CONFD_GET_INT16(newv));
		return ITER_CONTINUE;
	}

	if (tag != sys_timezone_name) {
		log_debug("sys_iter_timezone: unexpected tag %d", tag);
		return ITER_CONTINUE;
	}

	file = fopen(PATH_TIMEZONE_TMP, "w");
	if (!file)
		fatal("sys_iter_timezone: failed opening %s",
		    PATH_TIMEZONE_TMP);

	tz = CONFD_GET_CBUFPTR(newv);
	fprintf(file, "%s\n", tz);
	fclose(file);

	if (rename(PATH_TIMEZONE_TMP, PATH_TIMEZONE) < 0)
		log_warn("sys_iter_timezone: failed renaming %s to %s",
		    PATH_TIMEZONE_TMP, PATH_TIMEZONE);

	(void)unlink(PATH_LOCALTIME);
	snprintf(path, sizeof path, "%s/%s", PATH_ZONEINFO, tz);
	if (symlink(path, PATH_LOCALTIME) < 0)
		log_warn("sys_iter_timezone: failed symlinking %s",
		    PATH_LOCALTIME);

	return ITER_CONTINUE;
}

/* ARGSUSED */
enum cdb_iter_ret
sys_iter_hostname(confd_hkeypath_t *kp, enum cdb_iter_op op,
    confd_value_t *newv)
{
	FILE	*file;
	char	*hostname;
	size_t	 len;

	if (op == MOP_DELETED) {
		(void)unlink(PATH_HOSTNAME);
		return ITER_CONTINUE;
	}

	hostname = CONFD_GET_CBUFPTR(newv);
	len = CONFD_GET_BUFSIZE(newv);
	if (sethostname(hostname, len) < 0)
		fatal("sys_iter_hostname: sethostname");

	file = fopen(PATH_HOSTNAME, "w");
	if (!file)
		fatal("sys_iter_hostname: fopen " PATH_HOSTNAME);

	fprintf(file, "%s\n", hostname);
	fclose(file);

	return ITER_CONTINUE;
}

enum cdb_iter_ret
sys_iter_ntp(confd_hkeypath_t *kp, enum cdb_iter_op op, confd_value_t *newv)
{
	/* TODO */
	return ITER_CONTINUE;
}

/* ARGSUSED */
enum cdb_iter_ret
sys_iter_resolver(confd_hkeypath_t *kp, enum cdb_iter_op op,
    confd_value_t *newv)
{
	FILE		*file;
	int		 i, n;
	uint8_t		 timeout, attempts;
	uint16_t	 port;
	confd_value_t	 v;
	confd_value_t	*vs;
	char		 str[256];

	file = fopen(PATH_RESCONF, "w");
	if (!file)
		fatal("failed opening %s", PATH_RESCONF);

	cdb_pushd(ds, "/system/dns-resolver");
	if (cdb_exists(ds, "search")) {
		cdb_get_list(ds, &vs, &n, "search");
		fprintf(file, "search");
		for (i = 0; i < n; i++)
			fprintf(file, " %s", CONFD_GET_CBUFPTR(&vs[i]));
		fprintf(file, "\n");
		free(vs);
	}

	cdb_pushd(ds, "options");
	cdb_get_u_int8(ds, &timeout, "timeout");
	fprintf(file, "options timeout:%d\n", timeout);
	cdb_get_u_int8(ds, &attempts, "attempts");
	fprintf(file, "options attempts:%d\n", attempts);
	cdb_popd(ds);

	n = cdb_num_instances(ds, "server");
	if (n > 0) {
		for (i = 0; i < n; i++) {
			cdb_pushd(ds, "server[%d]", i);
			cdb_get(ds, &v, "udp-and-tcp/address");
			confd_pp_value(str, sizeof str, &v);
			fprintf(file, "nameserver %s", str);
			cdb_get_u_int16(ds, &port, "udp-and-tcp/port");
			if (port != 53)
				fprintf(file, ":%d", port);
			fprintf(file, "\n");
			cdb_popd(ds);
		}
	}

	cdb_popd(ds);
	fclose(file);
	return ITER_CONTINUE;
}

/* ARGSUSED */
enum cdb_iter_ret
sys_iter_radius(confd_hkeypath_t *kp, enum cdb_iter_op op,
    confd_value_t *newv)
{
	/* TODO */
	return ITER_CONTINUE;
}

/* ARGSUSED */
enum cdb_iter_ret
sys_iter_auth(confd_hkeypath_t *kp, enum cdb_iter_op op,
    confd_value_t *newv)
{
	struct passwd	 pw;
	struct spwd	 sp;
	int		 tag, i = kp->len - 2;
	char		*name;
	char		 path[256];
	char		 homedir[256] = "";
	char		 ssh_keydir[256] = "";
	char		 passwd[256] = "x";

	--i; tag = CONFD_GET_XMLTAG(&kp->v[i][0]);
	if (tag != sys_user)
		return ITER_CONTINUE;

	--i; name = CONFD_GET_CBUFPTR(&kp->v[i][0]);
	if (i > 0)
		return ITER_UP;

	memset(&pw, 0, sizeof pw);
	pw.pw_name = name;
	pw.pw_dir = homedir;
	pw.pw_shell = PATH_CONFD_CLI;
	pw.pw_passwd = passwd;

	snprintf(path, sizeof path, AAA_AUTH_USERS "/user{%s}", name);
	if (cdb_exists(ds, path)) {
		cdb_pushd(ds, path);
		cdb_get_int32(ds, (int32_t *)&pw.pw_uid, "uid");
		cdb_get_int32(ds, (int32_t *)&pw.pw_gid, "gid");
		cdb_get_str(ds, homedir, sizeof homedir, "homedir");
		cdb_get_str(ds, ssh_keydir, sizeof ssh_keydir, "ssh_keydir");
		cdb_popd(ds);
	} else if (op != MOP_DELETED) {
		log_warnx("sys_iter_auth: missing aaa user %s", name);
		return ITER_CONTINUE;
	}

	if (update_passwd(&pw, op) < 0) {
		log_warn("sys_iter_auth: update_passwd");
		return ITER_CONTINUE;
	}

	if (op != MOP_DELETED) {
		if (make_dir(homedir, 0750, pw.pw_uid, pw.pw_gid) < 0) {
			log_warn("sys_iter_auth: make_dir %s", homedir);
			return ITER_CONTINUE;
		}
		if (make_dir(ssh_keydir, 0700, pw.pw_uid, pw.pw_gid) < 0) {
			log_warn("sys_iter_auth: make_dir %s", ssh_keydir);
			return ITER_CONTINUE;
		}
	}

	/* produce path /system/authentication/user{name} */
	confd_pp_kpath_len(path, sizeof path, kp, 4);
	cdb_pushd(ds, path);

	if (write_authorized_keys(ssh_keydir, pw.pw_uid, pw.pw_gid) < 0)
		log_warn("ssh_iter_auth: write_authorized_keys");

	if (cdb_exists(ds, "password"))
		cdb_get_str(ds, passwd, sizeof passwd, "password");
	else
		passwd[0] = '!';

	cdb_popd(ds);

	memset(&sp, 0, sizeof sp);
	sp.sp_namp = name;
	sp.sp_pwdp = passwd;
	sp.sp_lstchg = time(NULL) / (60 * 60 * 24);
	sp.sp_min = 0;
	sp.sp_max = 99999;
	sp.sp_warn = 7;
	sp.sp_inact = -1;
	sp.sp_expire = -1;
	sp.sp_flag = -1;

	if (update_shadow(&sp, op) < 0)
		log_warn("sys_iter_auth: update_shadow");

	return ITER_CONTINUE;
}

int
make_dir(char *path, mode_t mode, uid_t uid, gid_t gid)
{
	struct stat	st;

	if (stat(path, &st) == 0)
		return (0);
	else if (errno != ENOENT)
		return (-1);

	if (mkdir(path, mode) < 0)
		return (-1);

	if (chown(path, uid, gid) < 0)
		return (-1);

	return (0);
}

int
update_passwd(struct passwd *new, enum cdb_iter_op op)
{
	struct passwd	*pw;
	FILE		*file;
	int		 found = 0, saved_errno;

	/* Delete the old backup */
	if (unlink(PATH_PASSWD "-") < 0 && errno != ENOENT)
		return (-1);

	/* Create a backup */
	if (link(PATH_PASSWD, PATH_PASSWD "-") < 0)
		return (-1);

	file = fopen(PATH_PASSWD "+", "w");
	if (!file)
		return (-1);

	setpwent();
	while ((pw = getpwent())) {
		if (strcmp(pw->pw_name, new->pw_name) == 0) {
			found = 1;
			if (op == MOP_DELETED)
				continue;
			pw = new;
		}
		if (putpwent(pw, file) < 0) {
			saved_errno = errno;
			endpwent();
			fclose(file);
			errno = saved_errno;
			return (-1);
		}
	}
	endpwent();

	/* Append the new entry if it wasn't found */
	if (!found && op != MOP_DELETED && putpwent(new, file) < 0) {
		saved_errno = errno;
		fclose(file);
		errno = saved_errno;
		return (-1);
	}
	fclose(file);

	if (rename(PATH_PASSWD "+", PATH_PASSWD) < 0)
		return (-1);

	return (0);
}

int
update_shadow(struct spwd *new, enum cdb_iter_op op)
{
	struct spwd	*sp;
	FILE		*file;
	int		 found = 0, saved_errno;
	mode_t		 old_mode;

	/* Delete the old backup */
	if (unlink(PATH_SHADOW "-") < 0 && errno != ENOENT)
		return (-1);

	/* Create a backup */
	if (link(PATH_SHADOW, PATH_SHADOW "-") < 0)
		return (-1);

	old_mode = umask(066);
	file = fopen(PATH_SHADOW "+", "w");
	saved_errno = errno;
	(void)umask(old_mode);
	if (!file) {
		errno = saved_errno;
		return (-1);
	}

	setspent();
	while ((sp = getspent())) {
		if (strcmp(sp->sp_namp, new->sp_namp) == 0) {
			found = 1;
			if (op == MOP_DELETED)
				continue;
			sp = new;
		}
		if (putspent(sp, file) < 0) {
			saved_errno = errno;
			endspent();
			fclose(file);
			errno = saved_errno;
			return (-1);
		}
	}
	endspent();

	/* Append the new entry if it wasn't found */
	if (!found && op != MOP_DELETED && putspent(new, file) < 0) {
		saved_errno = errno;
		fclose(file);
		errno = saved_errno;
		return (-1);
	}
	fclose(file);

	if (rename(PATH_SHADOW "+", PATH_SHADOW) < 0)
		return (-1);

	return (0);
}

int
write_authorized_keys(char *ssh_keydir, uid_t uid, gid_t gid)
{
	FILE		*file;
	char		 path[256];
	char		 keyname[256];
	uint8_t		*keydata;
	uint8_t		*base64;
	char		 algo[64];
	int		 i, n, len, saved_errno;
	mode_t		 old_mode;

	if (ssh_keydir[0] == '\0')
		return (0);

	snprintf(path, sizeof path, "%s/authorized_keys", ssh_keydir);

	n = cdb_num_instances(ds, "authorized-key");
	if (n < 1) {
		if (access(path, F_OK) == 0)
			(void)unlink(path);
		return (0);
	}

	old_mode = umask(066);
	file = fopen(path, "w");
	saved_errno = errno;
	(void)umask(old_mode);
	if (!file) {
		errno = saved_errno;
		return (-1);
	}

	for (i = 0; i < n; i++) {
		cdb_pushd(ds, "authorized-key[%d]", i);
		cdb_get_str(ds, keyname, sizeof keyname, "name");
		cdb_get_str(ds, algo, sizeof algo, "algorithm");
		cdb_get_binary(ds, &keydata, &len, "key-data");
		base64 = base64_encode(keydata, len, NULL);
		fprintf(file, "%s %s %s\n", algo, base64, keyname);
		free(base64);
		free(keydata);
		cdb_popd(ds);
	}

	fclose(file);
	return chown(path, uid, gid);
}

int
sys_get_elem(struct confd_trans_ctx *tctx, confd_hkeypath_t *kp)
{
	struct confd_datetime	 dt;
	struct sysinfo		 info;
	struct utsname		 uts;
	struct tm		*tm;
	confd_value_t		 v;
	time_t			 t;
	int			 tag, i = kp->len;

	--i; tag = CONFD_GET_XMLTAG(&kp->v[i][0]);
	if (tag != sys_system_state) {
		confd_data_reply_not_found(tctx);
		return CONFD_OK;
	}

	--i; tag = CONFD_GET_XMLTAG(&kp->v[i][0]);
	switch (tag) {
	case sys_clock:
		(void)time(&t);
		--i; tag = CONFD_GET_XMLTAG(&kp->v[i][0]);
		switch (tag) {
		case sys_boot_datetime:
			(void)sysinfo(&info);
			t -= info.uptime;
			/* FALLTHROUGH */
		case sys_current_datetime:
			tm = localtime(&t);
			assert(tm);
			dt.year		= tm->tm_year + 1900;
			dt.month	= tm->tm_mon + 1;
			dt.day		= tm->tm_mday;
			dt.hour		= tm->tm_hour;
			dt.min		= tm->tm_min;
			dt.sec		= tm->tm_sec;
			dt.micro	= 0;
			dt.timezone	= timezone / 3600;
			dt.timezone_minutes = (timezone % 3600) / 60;
			CONFD_SET_DATETIME(&v, dt);
			confd_data_reply_value(tctx, &v);
			return CONFD_OK;
		}
		break;
	case sys_platform:
		(void)uname(&uts);
		--i; tag = CONFD_GET_XMLTAG(&kp->v[i][0]);
		switch (tag) {
		case sys_os_name:
			CONFD_SET_STR(&v, uts.sysname);
			break;
		case sys_os_version:
			CONFD_SET_STR(&v, uts.version);
			break;
		case sys_os_release:
			CONFD_SET_STR(&v, uts.release);
			break;
		case sys_machine:
			CONFD_SET_STR(&v, uts.machine);
			break;
		default:
			confd_data_reply_not_found(tctx);
			return CONFD_OK;
		}
		confd_data_reply_value(tctx, &v);
		return CONFD_OK;
	}
	confd_data_reply_not_found(tctx);
	return CONFD_OK;
}
